<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $fillable=[
        'code',
        'photo',
        'first_name',
        'middle_name',
        'last_name',
        'phone_number_mobile',
        'phone_number_work',
        'position',
        'email',
        'physical_address',
        'work_address',
        'national_id_number',
        'contract_duration',
        'pay_day',
        'pay_slip',
        'gross',
        'net',

        'national_id',
        'passport_id',
        'licence_id',
        'other_id',
        'contract',
        'bank_statement',
        'bank_name',
        'bank_service_center',
        'bank_account_name',
        'bank_account_number',
//        'reference_letter',
        'co_workers',
        'consent',
        'progress',
        'score',
        'subscription',
        'terms_and_conditions',
        'amount',
        'approved_date',
        'due_date',
        'user_id',

    ];

    protected $hidden=[
        'photo',
        'first_name',
        'middle_name',
        'last_name',
        'phone_number_mobile',
        'phone_number_work',
        'position',
        'email',
        'physical_address',
        'work_address',
        'national_id_number',
        'contract_duration',
        'pay_day',
        'pay_slip',
        'gross',
        'net',
        'national_id',
        'passport_id',
        'licence_id',
        'other_id',
        'contract',
        'bank_statement',
        'bank_name',
        'bank_service_center',
        'bank_account_name',
        'bank_account_number',
//        'reference_letter',
        'co_workers',
        'consent',

        'score',
        'subscription',
        'terms_and_conditions',

        'user_id',
        'created_at',
        'updated_at',

    ];
}
