<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function employer()
    {
        return $this->belongsTo('App\Employer');
    }

    protected $fillable=[
        'photo',
        'first_name',
        'middle_name',
        'last_name',
        'phone_number_mobile',
        'phone_number_work',
        'position',
        'email',
        'physical_address',
        'work_address',
        'national_id_number',
        'contract_duration',
        'employer_id',
    ];

    protected $hidden=[
        'employer'
    ];
}
