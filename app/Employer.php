<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    public function employees()
    {
        return $this->hasMany('App\Employee');
    }
    public function users()
    {
        return $this->hasMany('App\User');
    }

    protected $fillable=[
        'name',
        'address',
        'letter',
    ];

    protected $hidden=[
      'employees'
    ];
}
