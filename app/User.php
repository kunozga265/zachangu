<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    public function employer()
    {
        return $this->belongsTo('App\Employer');
    }

    public function loans()
    {
        return $this->hasMany('App\Loan');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $fillable=[
        'first_name',
        'middle_name',
        'last_name',
        'national_id_number',
        'email',
        'password',
        'access_token',
        'employer_id',
        'subscription'
    ];

    protected $hidden=[
        'password',
        'id',
    ];
}
