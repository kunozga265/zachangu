<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class LoanAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status=0;
        $now=Carbon::now()->getTimestamp();

        if($this->progress==1)
            $status=0;
        elseif($this->progress==2 && $now<$this->due_date)
            $status=1;
        elseif($this->progress==2 && $now>$this->due_date)
            $status=2;
        elseif($this->progress==3)
            $status=3;

        return [
            'code'                  =>$this->code,
            'firstName'             =>$this->first_name,
            'middleName'            =>$this->middle_name,
            'lastName'              =>$this->last_name,
            'amount'                =>$this->amount,
            'status'                =>$status,
            'dueDate'               =>$this->due_date!=null?date('jS F, Y',$this->due_date):null,
            'approvedDate'          =>$this->approved_date!=null?date('jS F, Y',$this->approved_date):null,
            'updatedAt'             =>date('jS F, Y',$this->updated_at->getTimestamp()),
        ];
    }
}
