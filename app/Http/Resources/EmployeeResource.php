<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //check contract validity
        $contractStatus=0;
        $now=Carbon::today();
        $contractDuration=Carbon::createFromTimestamp($this->contract_duration);
        $diff=$now->diff($contractDuration);

        if($diff->invert == 0)
            $contractStatus=1;

        return [
            'id'                    => $this->id,
            'photo'                 => $this->photo,
            'firstName'             => $this->first_name,
            'middleName'            => $this->middle_name,
            'lastName'              => $this->last_name,
            'phoneNumberMobile'     => $this->phone_number_mobile,
            'phoneNumberWork'       => $this->phone_number_work,
            'position'              => $this->position,
            'email'                 => $this->email,
            'physicalAddress'       => json_decode($this->physical_address),
            'workAddress'           => json_decode($this->work_address),
            'nationalIdNumber'      => $this->national_id_number,
            'contractDuration'      => $this->contract_duration,
            'contractDurationISO'   => $contractDuration->toDateTimeString(),
            'contractDurationDate'  => date('jS F, Y',$this->contract_duration),
            'contractStatus'        => $contractStatus,
        ];
    }
}
