<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Employee;
use Carbon\Carbon;

class LoanAdminViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status=0;
        $now=Carbon::now()->getTimestamp();

        if($this->progress==1)
            $status=0;
        elseif($this->progress==2 && $now<$this->due_date)
            $status=1;
        elseif($this->progress==2 && $now>$this->due_date)
            $status=2;
        elseif($this->progress==3)
            $status=3;

        $employee=Employee::where('national_id_number',$this->national_id_number)->first();

        //decoding physical address
        $employeePhysicalAddress=json_decode($employee->physical_address);
        $loanPhysicalAddress=json_decode($this->physical_address);

        //calculating score
        similar_text($employeePhysicalAddress->name,$loanPhysicalAddress->name,$physicalAddressName);
        similar_text($employeePhysicalAddress->box,$loanPhysicalAddress->box,$physicalAddressBox);
        similar_text($employeePhysicalAddress->location,$loanPhysicalAddress->location,$physicalAddressLocation);

        //decoding work address
        $employeeWorkAddress=json_decode($employee->work_address);
        $loanWorkAddress=json_decode($this->work_address);

        //calculating score
        //similar_text($employeeWorkAddress->name,$loanWorkAddress->name,$workAddressName);
        similar_text($employeeWorkAddress->box,$loanWorkAddress->box,$workAddressBox);
        similar_text($employeeWorkAddress->location,$loanWorkAddress->location,$workAddressLocation);

        //computing dates
        $employeeContractDuration=date('jS F, Y',$employee->contract_duration);
        $loanContractDuration=date('jS F, Y',$this->contract_duration);

        similar_text(substr($employeeContractDuration,0,-6),substr($loanContractDuration,0,-6),$contractDurationDate);


        similar_text($employee->first_name,$this->first_name,$firstName);
        similar_text($employee->middle_name,$this->middle_name,$middleName);
        similar_text($employee->last_name,$this->last_name,$lastName);
        similar_text($employee->email,$this->email,$email);
        similar_text($employee->phone_number_mobile,$this->phone_number_mobile,$phoneNumberMobile);
        similar_text($employee->phone_number_work,$this->phone_number_work,$phoneNumberWork);

        similar_text($employee->position,$this->position,$position);

//        //total score
//        $score=(
//                round($firstName)+
//                round($middleName)+
//                round($lastName)+
//                round($email)+
//                round($phoneNumberMobile)+
//                round($phoneNumberWork)+
//                round(($physicalAddressName + $physicalAddressBox + $physicalAddressLocation)/3)+
//                round($position)+
//                round(($workAddressName + $workAddressBox + $workAddressLocation)/3)+
//                round($contractDurationDate))/10;

        //changing date format for coWorker contract duration
        $coWorkers=json_decode($this->co_workers);
        $coWorkers->coWorkerContractDuration1=date('jS F, Y', Carbon::create($coWorkers->coWorkerContractDuration1)->getTimestamp());
//        $coWorkers->coWorkerContractDuration2=date('jS F, Y', Carbon::create($coWorkers->coWorkerContractDuration2)->getTimestamp());



        $time=Carbon::createFromTimestamp($this->contract_duration);
        return [
            'id'                    =>$this->id,
            'code'                  =>$this->code,

            //compare
            'firstName'             =>[
                'form'      =>  $this->first_name,
                'employee'  =>  $employee->first_name,
                'score'     =>  round($firstName)
            ],
            'middleName'            =>[
                'form'      =>  $this->middle_name,
                'employee'  =>  $employee->middle_name,
                'score'     =>  round($middleName)
            ],
            'lastName'              =>[
                'form'      =>  $this->last_name,
                'employee'  =>  $employee->last_name,
                'score'     =>  round($lastName)
            ],
            'email'                 =>[
                'form'      =>  $this->email,
                'employee'  =>  $employee->email,
                'score'     =>  round($email)
            ],
            'phoneNumberMobile'     =>[
                'form'      =>  $this->phone_number_mobile,
                'employee'  =>  $employee->phone_number_mobile,
                'score'     =>  round($phoneNumberMobile)
            ],
            'phoneNumberWork'       =>[
                'form'      =>  $this->phone_number_work,
                'employee'  =>  $employee->phone_number_work,
                'score'     =>  round($phoneNumberWork)
            ],
            'physicalAddress'       =>[
                'form'      =>  json_decode($this->physical_address),
                'employee'  =>  json_decode($employee->physical_address),
                'score'     =>  round(($physicalAddressName + $physicalAddressBox + $physicalAddressLocation)/3)
            ],
            'position'              =>[
                'form'      =>  $this->position,
                'employee'  =>  $employee->position,
                'score'     =>  round($position)
            ],
            'workAddress'           =>[
                'form'      =>  json_decode($this->work_address),
                'employee'  =>  json_decode($employee->work_address),
                'score'     =>  round((/*$workAddressName +*/ $workAddressBox + $workAddressLocation)/2)
            ],
            'contractDurationDate'  => [
                'form'      =>  date('jS F, Y',$this->contract_duration),
                'employee'  =>  date('jS F, Y',$employee->contract_duration),
                'score'     =>  round($contractDurationDate)
            ],
            'photo'                 =>[
                'form'      =>  $this->photo,
                'employee'  =>  $employee->photo,
            ],


//            'totalScore'            =>$score,
//            'score'                 =>$this->score,
            'nationalIdNumber'      =>$this->national_id_number,

            'contractDuration'      =>$this->contract_duration,
            'contractDurationISO'   =>$time->toDateTimeString(),

            'contract'              =>$this->contract,
            'payDay'                =>$this->pay_day,
            'paySlip'               =>$this->pay_slip,
//            'gross'                 =>$this->gross,
//            'net'                   =>$this->net,
            'referenceLetter'       =>$this->reference_letter,
            'coWorkers'            =>$coWorkers,
//            'bankStatement'         =>$this->bank_statement,
//            'bankName'              =>$this->bank_name,
//            'bankServiceCenter'     =>$this->bank_service_center,
//            'bankAccountName'       =>$this->bank_account_name,
//            'bankAccountNumber'     =>$this->bank_account_number,
            'nationalId'            =>$this->national_id,
            'passportId'            =>$this->passport_id,
            'licenceId'             =>$this->licence_id,
            'otherId'               =>$this->other_id,
            'consent'               =>$this->consent,
            'progress'              =>$this->progress,
            'score'                 =>$this->score,
            'terms_and_conditions'  =>$this->terms_and_conditions,
            'amount'                =>$this->amount,
            'dueDate'               =>$this->due_date!=null?date('jS F, Y',$this->due_date):null,
            'approvedDate'          =>$this->approved_date!=null?date('jS F, Y',$this->approved_date):null,
            'updatedAt'             =>date('jS F, Y',$this->updated_at->getTimestamp()),
//            'subscription'          =>$this->subscription,
            'status'                =>$status,
            'userId'                =>$this->user->id,
            'employeeId'            =>$employee->id,
        ];
    }
}
