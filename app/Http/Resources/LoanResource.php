<?php

namespace App\Http\Resources;

use App\Employee;
use App\Http\Controllers\LoanController;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Date;
use Carbon\Carbon;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status=0;
        $now=Carbon::now()->getTimestamp();

        if($this->progress==1) //loan has been applied
            $status=0;

        elseif($this->progress==2 && $now<$this->due_date) //loan is active and is not due
            $status=1;

        elseif($this->progress==2 && $now>=$this->due_date) //loan is active and is due
            $status=2;

        elseif($this->progress==3) //loan is closed
            $status=3;

        $employee=Employee::where('national_id_number',$this->national_id_number)->first();


        $time=Carbon::createFromTimestamp($this->contract_duration);
        return [
            'id'                    =>$this->id,
            'photo'                 =>$this->photo,
            'code'                  =>$this->code,
            'firstName'             =>$this->first_name,
            'middleName'            =>$this->middle_name,
            'lastName'              =>$this->last_name,
            'nationalIdNumber'      =>$this->national_id_number,
            'email'                 =>$this->email,
            'phoneNumberMobile'     =>$this->phone_number_mobile,
            'phoneNumberWork'       =>$this->phone_number_work,
            'physicalAddress'       =>json_decode($this->physical_address),
            'position'              =>$this->position,
            'workAddress'           =>json_decode($this->work_address),
            'contractDuration'      =>$this->contract_duration,
            'contractDurationISO'   =>$time->toDateTimeString(),
            'contractDurationDate'  => date('jS F, Y',$this->contract_duration),
            'contract'              =>$this->contract,
            'payDay'                =>$this->pay_day,
            'paySlip'               =>$this->pay_slip,
//            'gross'                 =>$this->gross,
//            'net'                   =>$this->net,
            'referenceLetter'       =>$this->reference_letter,
            'coWorkers'             =>json_decode($this->co_workers),
//            'bankStatement'         =>$this->bank_statement,
//            'bankName'              =>$this->bank_name,
//            'bankServiceCenter'     =>$this->bank_service_center,
//            'bankAccountName'       =>$this->bank_account_name,
//            'bankAccountNumber'     =>$this->bank_account_number,
            'nationalId'            =>$this->national_id,
            'passportId'            =>$this->passport_id,
            'licenceId'             =>$this->licence_id,
            'otherId'               =>$this->other_id,
            'consent'               =>$this->consent,
            'progress'              =>$this->progress,
            'score'                 =>$this->score,
            'terms_and_conditions'  =>$this->terms_and_conditions,
            'generate_terms_and_conditions'  => $this->progress==0?LoanController::generateTermsAndConditions($this):null,
            'amount'                =>$this->amount,
            'dueDate'               =>$this->due_date!=null?date('jS F, Y',$this->due_date):null,
            'approvedDate'          =>$this->approved_date!=null?date('jS F, Y',$this->approved_date):null,
            'updatedAt'             =>date('jS F, Y',$this->updated_at->getTimestamp()),
//            'subscription'          =>$this->subscription,
            'status'                =>$status,
            'userId'                =>$this->user->id,
            'employeeId'            =>$employee->id,
        ];
    }
}
