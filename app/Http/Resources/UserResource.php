<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        $admin=?true:false;
        if($this->email=='admin@zachanguloans.com'){
            return [
                'id'                                => $this->id,
                'firstName'                         => $this->first_name,
                'middleName'                        => $this->middle_name,
                'lastName'                          => $this->last_name,
                'nationalIdNumber'                  => $this->national_id_number,
                'email'                             => $this->email,
                'accessToken'                       => $this->access_token,
                'admin'                             => true,
            ];
        }else{
//            //check for eligibility
//            $employee=$this->employer->employees()->where('national_id_number',$this->national_id_number)->first();
////        $loans=$this->loans()->where('progress','<',3)->first();
//            $eligibility=is_object($employee)?1:0;
//
//            $contract_duration_eligibility='';
//            $contract_duration=0;
//
//
//            if ($eligibility===1){
//
//                $contract_duration=$employee->contract_duration;
////                $time=Carbon::createFromTimestamp($employee->contract_duration);
////                $now=Carbon::now();
////                $diff=$time->diff($now);
////
////
////                if($diff->invert == 1){
////                    if($diff->y > 0){
////                        $contract_duration_eligibility=1;
////                    }
////                    else{
////                        //if it's greater than a month then subtract one month from the current date
////                        if($diff->m >= 3){
////                            $contract_duration_eligibility=1;
//////                        }elseif($diff->m >= 1){
//////                            $contract_duration_eligibility=1;
//////                        }
//////                        else{
//////                            if($diff->d > 0){
//////                                $contract_duration_eligibility=1;
//////                            }
//////                            else{
//////                                $contract_duration_eligibility=0;
//////                            }
////                        } else{
////                            $contract_duration_eligibility=0;
////                        }
////                    }
////                }
//            } else {
////                    $contract_duration_eligibility=0;
//                $contract_duration=Carbon::today()->getTimestamp();
//            }

            return [
                'id'                                => $this->id,
                'firstName'                         => $this->first_name,
                'middleName'                        => $this->middle_name,
                'lastName'                          => $this->last_name,
                'nationalIdNumber'                  => $this->national_id_number,
                'email'                             => $this->email,
                'accessToken'                       => $this->access_token,
                'employer'                          => $this->employer,
                'loans'                             => $this->loans->count(),
//                'contract_duration_eligibility'     => $contract_duration_eligibility,
                'subscription'                      => $this->subscription,
                'admin'                             => false,
            ];
        }



    }
}
