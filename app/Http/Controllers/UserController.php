<?php

namespace App\Http\Controllers;

use App\Employer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\User;
use App\Http\Resources;
use Validator;
use Illuminate\Support\Facades\Auth;

//jwt-auth
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Http\Controllers\LoanController;

//send mail
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;

class UserController extends Controller
{
    public function index()
    {
        $users=User::where('email','!=','admin@zachanguloans.com')->orderBy('first_name','ASC')->paginate(10);
        return response()->json([
            'response'  =>  true,
            'users'  =>  new Resources\UserCollection($users)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'first_name'            => 'required',
            'last_name'             => 'required',
            'national_id_number'    => 'required|min:8|unique:users',
            'email'                 => 'required|unique:users',
            'password'              => 'required|min:8',
            'employer_id'           => 'required',

        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Missing Fields']);
        }

        $user=new User();
//        $name=str_slug($request->name);

        $user->first_name           = $request->first_name;
        $user->middle_name          = $request->middle_name;
        $user->last_name            = $request->last_name;
        $user->national_id_number   = strtoupper($request->national_id_number);
        $user->email                = $request->email;
        $user->employer_id          = $request->employer_id;
        $user->password             = bcrypt($request->password);

        $user->save();

        $token = JWTAuth::fromUser($user);
        $user->update([
            'access_token'=>$token
        ]);
        $employer_name=$user->employer->name;

        $data=[
            'subject'       =>  'New User',
            'email'         => 'admin@zachanguloans.com',
            'bodyMessage'   =>  "<div>Greetings!</div><br><br><div>$user->first_name $user->last_name has just registered into the system. Please ensure their details are available under $employer_name Employer.</div><br>
<div>Visit <a href='https://app.zachanguloans.com/'>Zachangu Loans</a> for more details.</div><br><br>
<div>Regards.</div>  ",
        ];

        $notification=LoanController::sendMail($data);

        return response()->json([
            'response'      =>  true,
            'notification'  =>  $notification,
            'user'          =>  new Resources\UserResource($user)
        ]);
    }

    public function show($id)
    {
        $user=User::find($id);

        if (is_object($user)){
            $response=[
                'response' =>true,
                'user'=>new Resources\UserResource($user),
            ];
            return response()->json($response,200);
        }
        return response()->json(['response' =>  false]);
    }

    public function eligibility ($id)
    {
        $user=User::find($id);

        if (is_object($user)){
            //check for eligibility whether the employee data is available
            $employee=$user->employer->employees()->where('national_id_number',$user->national_id_number)->first();
            $employeeEligibility=is_object($employee)?true:false;

            //contract duration
            $contract_duration_eligibility=false;
            $contract_duration='';

            //current time
            $now=Carbon::now();

            //if the employee's data is available under employer
            if ($employeeEligibility){
                $contract_duration=$employee->contract_duration;
                $time=Carbon::createFromTimestamp($contract_duration);

                $diff=$time->diff($now);

                //if the contract date is ahead
                if($diff->invert == 1){
                    //eligible if the contract date is greater than a year
                    if($diff->y > 0){
                        $contract_duration_eligibility=true;
                    }
                    else{
                        //eligible if the contract date is equal or greater than 3 months
                        if($diff->m >= 3) {
                            $contract_duration_eligibility = true;
//                        }elseif($diff->m >= 1){
//                            $contract_duration_eligibility=true;
//                        }
//                        else{
//                            if($diff->d > 0){
//                                $contract_duration_eligibility=true;
//                            }
//                            else{
//                                $contract_duration_eligibility=false;
//                            }
//                        }
                            //eligible if contract expires within 3 months
                        }else{
                                $contract_duration_eligibility=false;
                            }
                    }
                }
                //not eligible if contract has expired
                else {
                    $contract_duration_eligibility=false;
                    $contract_duration=0;
                }
            }

            //whether user already has a loan
            $loans=$user->loans()->where('progress','<',3)->get();
            $existingLoanEligibility=$loans->count()==0?true:false;

//            dd( $employeeEligibility, $contract_duration_eligibility, $existingLoanEligibility);

            //get subscription eligibility
            $subscriptionEligibility = $now->getTimestamp() <= $user->subscription ? true:false;

            $response=[
                'response' =>true,
                'eligibility'=>[
                    'status'=>$employeeEligibility&&$contract_duration_eligibility&&$existingLoanEligibility,
                    'employee'=>$employeeEligibility,
                    'contract'=>$contract_duration_eligibility,
                    'contractDuration'=>$contract_duration,
                    'loan'=>$existingLoanEligibility,
                    'subscription'=> $subscriptionEligibility
                ]
            ];
            return response()->json($response,200);
        }
        return response()->json(['response' =>  false]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'email'         => 'required',
            'employer_id'   => 'required',
            'password'      => 'required|min:8',
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>4]); //missing fields
        }

        if(Auth::attempt([
            'email'      => $request->email,
            'password'   => $request->password,
        ])){
            if ($request->email=='admin@zachanguloans.com'){
                $user=User::where('email',$request->email)->first();
            }else{
                $employer=Employer::find($request->employer_id);
                if (is_object($employer)) {
                    $user = $employer->users()->where('email', $request->email)->first();
                }else
                    return response()->json(['response'=>false,'status'=>7]);

            }

            if (is_object($user)){
                $token = JWTAuth::fromUser($user);

                $user->update([
                    'access_token'=>$token
                ]);

                $response=[
                    'response'  =>  true,
                    'user'      =>  new Resources\UserResource($user)
                ];
                return response()->json($response,200);
            }else{
                return response()->json(['response'=>false,'status'=>6]); //user doesn't belong to this employer
            }

        }else
            return response()->json(['response'=>false,'status'=>5]); //invalid credentials


//        if (Auth::attempt($credentials)){
//            $user=User::where('username',$request->username)->first();
//
//            if ($user){
//                $token = JWTAuth::fromUser($user);
//
//                $user->update([
//                    'access_token'=>$token
//                ]);
//            }
//
//            $response=[
//                'response'  =>  true,
//                'user'      =>  new Resources\UserResource($user)
//            ];
//            return response()->json($response,200);
//        }
//        return response()->json(['response'=>false]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user=User::find($id);

        if(is_object($user)){
            $response=[];

            if($request->type=='admin'){
                if(isset($request->new_password)&&isset($request->old_password)){
                    if(Auth::attempt([
                        'email'   => $request->old_email,
                        'password'   => $request->old_password,
                    ])) {
                        $token = JWTAuth::fromUser($user);

                        $user->update([
                            'password'              => bcrypt($request->new_password),
                            'access_token'          => $token
                        ]);

                        $response=[
                            'response'  =>  true,
                            'user'      =>  new Resources\UserResource($user)
                        ];
                    }else{
                        $response=[
                            'response'  =>  false,
                            'status'      =>  5 //password didnt match
                        ];
                    }
                }else{
                    $response=[
                        'response'  =>  false,
                        'status'      =>  4 //password fields not available
                    ];
                }

            }else{
                $validator=Validator::make($request->all(),[
                    'first_name'            => 'required',
                    'last_name'             => 'required',
                    'old_email'             => 'required',
                    'national_id_number'    => 'required',
                ]);

                if($validator->fails()){
                    return response()->json(['response'=>false, 'status'=>4]);
                }

                if(isset($request->new_password)&&isset($request->old_password)){
                    if(Auth::attempt([
                        'email'   => $request->old_email,
                        'password'   => $request->old_password,
                    ])) {
                        $token = JWTAuth::fromUser($user);

                        $user->update([
                            'first_name'            => $request->first_name,
                            'middle_name'           => $request->middle_name,
                            'last_name'             => $request->last_name,
                            'email'                 => isset($request->new_email)?$request->new_email:$request->old_email,
                            'national_id_number'    => $request->national_id_number,
                            'password'              => bcrypt($request->new_password),
                            'access_token'          => $token
                        ]);

                        $response=[
                            'response'  =>  true,
                            'user'      =>  new Resources\UserResource($user)
                        ];
                    }else{
                        $response=[
                            'response'  =>  false,
                            'status'      =>  5 //password didnt match
                        ];
                    }
                }else{
                    $token = JWTAuth::fromUser($user);

                    $user->update([
                        'first_name'            => $request->first_name,
                        'middle_name'           => $request->middle_name,
                        'last_name'             => $request->last_name,
                        'national_id_number'    => $request->national_id_number,
                        'email'                 => isset($request->new_email)?$request->new_email:$request->old_email,
                        'access_token'          => $token
                    ]);
                    $response=[
                        'response'  =>  true,
                        'user'      =>  new Resources\UserResource($user)
                    ];
                }
            }

            return response()->json($response,200);
        }else{
            return response()->json([
                'response'      =>  false,
                'status'      =>  6,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        if(is_object($user)){

            $user->delete();

//            $users=User::all();
            return response()->json([
                'response'  =>  true,
//                'users'  =>  new Resources\UserCollection($users)
            ]);
        }
        return response()->json([
            'response'  =>false,
        ],400);
    }

    public function feedback(Request $request){
        $this->validate($request,[
            'message'   =>  'required',
            'email'     =>  'required',
            'name'      =>  'required',
        ]);

        $data=[
            'subject'       =>  'Contact Form',
            'email'         =>  $request->email,
            'name'          =>  $request->name,
            'bodyMessage'   =>  $request->message
        ];




        // Create the Transport
        $transport = (new Swift_SmtpTransport('mail.zachanguloans.com', 465,'ssl'))
            ->setUsername('admin@zachanguloans.com')
            ->setPassword('5E;dRxjC4:1Y9i')
        ;

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message($data['subject']))
            ->setFrom('system@zachanguloans.com')
//            ->setTo(['pnt-17bdec@inbox.mailtrap.io'])
            ->setTo(['admin@zachanguloans.com'])
            ->setBody("
<div>New message from contact us form!</div><br><br>
<div>$data[name]<br>$data[email]</div><br><br>
<div>$data[bodyMessage]</div>",'text/html')
        ;

        // Send the message
        try{
            $result = $mailer->send($message);
            //if successful
            if($result==1)
                return response()->json(['response' => true]);
            else
                return response()->json(['response' => false,'status'=>5]);
        }catch (\Swift_TransportException $exception){
            return response()->json(['response' => false,'status'=>6]);
        }
    }
}
