<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Employer;
use Illuminate\Http\Request;
use App\Http\Resources;
use Validator;

class EmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employers=Employer::orderBy('name','ASC')->get();
        if (is_object($employers)){
            return response(['response'=>true,'employers'=>Resources\EmployerResource::collection($employers)],200);
        }

        return response(['response'=>false,'message'=>'No employers'],400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'      => 'required',
            'address'   => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Missing Fields']);
        }

        $employer=new Employer([
            'name'      =>  $request->name,
            'address'   =>  $request->address,
            'letter'   =>  $request->letter
        ]);
        $employer->save();

        return response(['response'=>true,'employerData'=>$employer->id],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employer=Employer::find($id);

        if (is_object($employer)){
            $response=[
                'response' =>true,
                'employer'=>new Resources\EmployerResource($employer),
                'employees'=>Resources\EmployeeResource::collection($employer->employees()->orderBy('first_name','ASC')->get())
            ];
            return response()->json($response,200);
        }
        return response()->json(['response' =>  false]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employer=Employer::find($id);

        if (is_object($employer)){

            $employer->update([
                'name'      =>  $request->name,
                'address'   =>  $request->address,
                'letter'   =>  $request->letter
            ]);

            return response(['response'=>true,'employerData'=>$employer->id],200);
        }
        return response(['response'=>false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employer=Employer::find($id);

        if (is_object($employer)){
            $employer->delete();
            return response(['response'=>true],200);
        }
        return response(['response'=>false]);
    }
}
