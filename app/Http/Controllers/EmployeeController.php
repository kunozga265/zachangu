<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Http\Resources;
use Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees=Employee::all();
        if (is_object($employees)){
            return response(['response'=>true,'employees'=>new Resources\EmployeeCollection($employees)],200);
        }

        return response(['response'=>false,'message'=>'No employees'],400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'first_name'            => 'required',
            'last_name'             => 'required',
            'position'              => 'required',
            'email'                 => 'required',
            'national_id_number'    => 'required|unique:employees',
            'contract_duration'     => 'required',
            'employer_id'           => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Invalid Fields']);
        }

        $employee=new Employee([
            'photo'                 => $request->photo,
            'first_name'            => $request->first_name,
            'middle_name'           => $request->middle_name,
            'last_name'             => $request->last_name,
            'phone_number_mobile'   => $request->phone_number_mobile,
            'phone_number_work'     => $request->phone_number_work,
            'position'              => $request->position,
            'email'                 => $request->email,
            'physical_address'      => json_encode($request->physical_address),
            'work_address'          => json_encode($request->work_address),
            'national_id_number'    => strtoupper($request->national_id_number),
            'contract_duration'     => $request->contract_duration,
            'employer_id'           => $request->employer_id,
            ]);

        $employee->save();

        return response(['response'=>true],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee=Employee::find($id);

        if (is_object($employee)){
            $response=[
                'response' =>true,
                'employee'=>new Resources\EmployeeResource($employee),
            ];
            return response()->json($response,200);
        }
        return response()->json(['response' =>false]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee=Employee::find($id);

        if (is_object($employee)){
            $validator=Validator::make($request->all(),[
                'first_name'            => 'required',
                'last_name'             => 'required',
                'position'              => 'required',
                'email'                 => 'required',
                'national_id_number'    => 'required',
                'contract_duration'     => 'required',
            ]);

            if($validator->fails()){
                return response()->json(['response'=>false, 'status'=>'Invalid Fields']);
            }

            $employee->update([
                'photo'                 => $request->photo,
                'first_name'            => $request->first_name,
                'middle_name'           => $request->middle_name,
                'last_name'             => $request->last_name,
                'phone_number_mobile'   => $request->phone_number_mobile,
                'phone_number_work'     => $request->phone_number_work,
                'position'              => $request->position,
                'email'                 => $request->email,
                'physical_address'      => json_encode($request->physical_address),
                'work_address'          => json_encode($request->work_address),
                'national_id_number'    => strtoupper($request->national_id_number),
                'contract_duration'     => $request->contract_duration,
            ]);

            return response(['response'=>true,'employeeData'=>$employee->employer->id],200);
        }
        return response(['response'=>false,'status'=>0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee=Employee::find($id);

        if (is_object($employee)){
            $employee->delete();
            return response(['response'=>true],200);
        }
        return response(['response'=>false, 'status'=>0]);
    }
}
