<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Loan;
use App\Page;
use App\User;
use Illuminate\Support\Facades\App;
use PDF;
use Illuminate\Http\Request;
use App\Http\Resources;
use Validator;
use Storage;
use Carbon\Carbon;

use App\Exports\LoanExport;
use Maatwebsite\Excel\Facades\Excel;

//send mail
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAdmin()
    {
        $now=Carbon::now()->getTimestamp();

        $pending_loans=Loan::where('progress',1)->get();
        $active_loans=Loan::where('progress',2)->where('due_date','>',$now)->get();
        $due_loans=Loan::where('progress',2)->where('due_date','<',$now)->get();

        $response=[
            'response'  =>  true,
            'loans'     =>  [
                'pending'   =>  Resources\LoanAdminResource::collection($pending_loans),
                'active'    =>  Resources\LoanAdminResource::collection ($active_loans),
                'due'    =>  Resources\LoanAdminResource::collection ($due_loans),
            ]
        ];
        return response($response,200);
//
//        if (is_object($loans)){
//            return response(['response'=>true,'loans'=>new Resources\LoanCollection($loans)],200);
//        }
//
//        return response(['response'=>false,'message'=>'No loans'],400);
    }
    public function indexUserAdmin($user_id)
    {
        $user=User::find($user_id);
        $loans=$user->loans()->orderBy('updated_at','ASC')->paginate(10);

        if (is_object($loans)){
            return response(['response'=>true,'loans'=>new Resources\LoanCollection($loans)],200);
        }

        return response(['response'=>false,'message'=>'No loans']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
        $user=User::find($user_id);

        $loans=$user->loans()->where('progress',3)->get();
        if (is_object($loans)){
            return response(['response'=>true,'loans'=>$loans],200);
        }

        return response(['response'=>false,'message'=>'No loans'],400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
//            'first_name'            => 'required',
//            'last_name'             => 'required',
//            'position'              => 'required',
//            'email'                 => 'required',
//            'work_address'          => 'required',
            'national_id_number'    => 'required|min:8|max:8',
//            'contract_duration'     => 'required',
            'user_id'               => 'required',
            'amount'                => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Missing Fields']);
        }

        $loan=new Loan([
            'code'                  => strtolower($request->national_id_number).uniqid(),
            'photo'                 => $request->photo,
            'first_name'            => $request->first_name,
            'middle_name'           => $request->middle_name,
            'last_name'             => $request->last_name,
            'phone_number_mobile'   => $request->phone_number_mobile,
            'phone_number_work'     => $request->phone_number_work,
            'position'              => $request->position,
            'email'                 => $request->email,
            'physical_address'      => json_encode($request->physical_address),
            'work_address'          => json_encode($request->work_address),
            'national_id_number'    => strtoupper($request->national_id_number),
            'contract_duration'     => $request->contract_duration,
            'user_id'               => $request->user_id,

            'national_id'           => $request->national_id,
            'passport_id'           => $request->passport_id,
            'licence_id'            => $request->licence_id,
            'other_id'              => $request->other_id,
            'contract'              => $request->contract,
            'pay_day'               => $request->pay_day,
            'pay_slip'              => $request->pay_slip,
//            'gross'                 => $request->gross,
//            'net'                   => $request->net,
//            'bank_statement'        => $request->bank_statement,
//            'bank_name'             => $request->bank_name,
//            'bank_service_center'   => $request->bank_service_center,
//            'bank_account_name'     => $request->bank_account_name,
//            'bank_account_number'   => $request->bank_account_number,
            'reference_letter'      => $request->reference_letter,
            'co_workers'            => json_encode($request->co_workers),
            'progress'              => 0,
            'amount'                => $request->amount,
            //'subscription'          => 0

        ]);

        $loan->save();

        $loan->update([
            'score'=>$this->getScore($loan)
        ]);

        //subscribe for the next three months
        $user=User::find($request->user_id);
        $user->update([
            'subscription'=>Carbon::today()->addMonth(3)->getTimestamp()
        ]);


        return response(['response'=>true,'loanData'=>$loan->code, "subscription"=>$user->subscription],200);
    }

    public function storeSubscribed(Request $request)
    {
        $validator=Validator::make($request->all(),[
//            'national_id_number'    => 'required|min:8|max:8',
            'user_id'               => 'required',
            'amount'                => 'required',
        ]);

        if($validator->fails()){
            return response()->json(['response'=>false, 'status'=>'Missing Fields']);
        }

        $user=User::find($request->user_id);
        $currentLoans=$user->loans()->where('progress','<',3)->get();
        $today=Carbon::today();
        //check if the subscription is valid
        if($user->subscription != null && $today->getTimestamp() <= $user->subscription && $currentLoans->count()==0){
            //get when the subscription ends
            $subscribedDate=Carbon::createFromTimestamp($user->subscription);

            //getting latest loan details
            $loanDetails=$user->loans()
                ->where('created_at','>=',$subscribedDate->subMonth(3)->toDateTimeString()) //within subscribed period
                ->where('progress',3) //it has been paid back
                ->latest()
                ->first();

            $loan=new Loan([
                'code'                  => strtolower($loanDetails->national_id_number).uniqid(),
                'photo'                 => $loanDetails->photo,
                'first_name'            => $loanDetails->first_name,
                'middle_name'           => $loanDetails->middle_name,
                'last_name'             => $loanDetails->last_name,
                'phone_number_mobile'   => $loanDetails->phone_number_mobile,
                'phone_number_work'     => $loanDetails->phone_number_work,
                'position'              => $loanDetails->position,
                'email'                 => $loanDetails->email,
                'physical_address'      => $loanDetails->physical_address,
                'work_address'          => $loanDetails->work_address,
                'national_id_number'    => strtoupper($loanDetails->national_id_number),
                'contract_duration'     => $loanDetails->contract_duration,
                'user_id'               => $loanDetails->user_id,

                'national_id'           => $loanDetails->national_id,
                'passport_id'           => $loanDetails->passport_id,
                'licence_id'            => $loanDetails->licence_id,
                'other_id'              => $loanDetails->other_id,
                'contract'              => $loanDetails->contract,
                'pay_day'               => $loanDetails->pay_day,
                'pay_slip'              => $loanDetails->pay_slip,
//                'gross'                 => $loanDetails->gross,
//                'net'                   => $loanDetails->net,
//                'bank_statement'        => $loanDetails->bank_statement,
//                'bank_name'             => $loanDetails->bank_name,
//                'bank_service_center'   => $loanDetails->bank_service_center,
//                'bank_account_name'     => $loanDetails->bank_account_name,
//                'bank_account_number'   => $loanDetails->bank_account_number,
                'reference_letter'      => $loanDetails->reference_letter,
                'co_workers'            => $loanDetails->co_workers,
                'score'                 => $loanDetails->score,
                'progress'              => 0,
                'amount'                => $request->amount
            ]);

            $loan->save();

            return response(['response'=>true,'loanData'=>$loan->code],200);

        }else
            return response(['response'=>false],200);


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $code, $type)
    {


       /* if($type=='approve'){
            $loan=Loan::where('code',$code)->first();
            if (is_object($loan)){

                return response()->json([
                    'response' =>true,
                    'loan'=> new Resources\LoanApproveResource($loan)
                ],200);
            }else
                return response()->json(['response' =>false]);

        } else*/ if($type=='admin'){
            $loan=Loan::where('code',$code)->where('progress','>',0)->first();
            if (is_object($loan)){

                return response()->json([
                    'response' =>true,
                    'loan'=> new Resources\LoanAdminViewResource($loan)
                ],200);
            }else
                return response()->json(['response' =>false]);

        }else{
            $user=User::find($user_id);

            if ($type=='view'){
                $loan=$user->loans()->where('code',$code)->first();
            }else{
                $loan=$user->loans()->where('progress','<',3)->first();
            }


            if (is_object($loan)){
//                if($loan->progress==0){
//                    //get score
//                    $loan->update([
//                        'score'=>70
//                    ]);
//                }

//                //set subscription
//                $time=Carbon::createFromTimestamp($loan->contract_duration);
//                $now=Carbon::now();
//                $diff=$time->diff($now);
//
//                if($diff->y > 0){
//                    $loan->update([
//                        'subscription'=>2
//                    ]);
//                }
//                else{
//                    //if it's greater than a month then subtract one month from the current date
//                    if($diff->m >= 6){
//                        $loan->update([
//                            'subscription'=>2
//                        ]);
//                    }
//                    else if($diff->m >= 3){
//                        $loan->update([
//                            'subscription'=>1
//                        ]);
//                    }
//                    else{
//                        $loan->update([
//                            'subscription'=>0
//                        ]);
//                    }
//                }

                if ($type=='view'){
                    $response=[
                        'response' =>true,
                        'loan'=> new Resources\LoanResource($loan)
                    ];
                }else{
                    $response=[
                        'response' =>true,
                        'loan'=> $loan
                    ];
                }



                return response()->json($response,200);
            }
            return response()->json(['response' =>false]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modify ($code, $type)
    {
        $loan=Loan::where('code',$code)->first();

        if (is_object($loan)){
            if ($type=='approve'){
                $today=Carbon::now();
//                $due_date=Carbon::now()->addMonth();
                $due_date=Carbon::createFromDate(null,null,$loan->pay_day);

                $diff=$due_date->diff($today);

                //due date is behind
                if($diff->invert == 0) {
                    $due_date->addMonth();
                }
                //check if the contract date is behind or not
                $contract_duration=Carbon::createFromTimestamp($loan->contract_duration);
                $diff=$due_date->diff($contract_duration);

                //if the contract date is behind
                if($diff->invert == 1) {
                    $due_date=$contract_duration;
                }

                $loan->update([
                    'approved_date'=>$today->getTimestamp(),
                    'due_date'=>$due_date->getTimestamp(),
                    'progress'=>2,
                ]);

                $page=Page::first();
                $contents=json_decode($page->contents);
                $due_date=$due_date->format('jS F Y');
                $totalAmount=($loan->amount)*($contents->interest + 1) + $contents->fee;

                $data=[
                    'subject'       =>  'Loan Approval',
                    'email'         => $loan->email,
                    'bodyMessage'   =>  "
<div>Dear $loan->first_name $loan->last_name</div>
<div>Zachangu Loans is pleased to inform you that your loan of MK$loan->amount has been approved. On $due_date, an amount of MK$totalAmount will be deducted from your account number $loan->bank_account_number held at $loan->bank_name.</div><br>
<div>You may contact us for more details.</div><br><br>
<div>Regards.</div>  ",
                ];

                dd($data);

                $notification=$this->sendMail($data);

                return response(['response'=>true,'notification'=>$notification,'loan'=>$loan],200);

            }else {
                $loan->update([
                    'progress'=>3,
                ]);

            }
        }else
            return response(['response'=>false]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id, $code)
    {
        $user=User::find($user_id);
        $loan=$user->loans()->where('code',$code)->first();

        if (is_object($loan)){

            switch ($request->type){
                case 'full':
                    $loan->update([
                        'photo'                 => $request->photo,
                        'first_name'            => $request->first_name,
                        'middle_name'           => $request->middle_name,
                        'last_name'             => $request->last_name,
                        'phone_number_mobile'   => $request->phone_number_mobile,
                        'phone_number_work'     => $request->phone_number_work,
                        'position'              => $request->position,
                        'email'                 => $request->email,
                        'physical_address'      => json_encode($request->physical_address),
                        'work_address'          => json_encode($request->work_address),
                        'contract_duration'     => $request->contract_duration,
                        'pay_day'               => $request->pay_day,
                        'pay_slip'              => $request->pay_slip,
                        'gross'                 => $request->gross,
                        'net'                   => $request->net,
                        'reference_letter'      => $request->reference_letter,
                        'co_workers'            => json_encode($request->co_workers),
                        'national_id'           => $request->national_id,
                        'passport_id'           => $request->passport_id,
                        'licence_id'            => $request->licence_id,
                        'other_id'              => $request->other_id,
                        'contract'              => $request->contract,
                        'bank_statement'        => $request->bank_statement,
                        'bank_name'             => $request->bank_name,
                        'bank_service_center'   => $request->bank_service_center,
                        'bank_account_name'     => $request->bank_account_name,
                        'bank_account_number'   => $request->bank_account_number,

                        'amount'                =>  $request->amount
                    ]);

                    $loan->update([
                        'score'=>$this->getScore($loan)
                    ]);

                    return response(['response'=>true,'loanData'=>$loan->code],200);
                    break;
//                case 'amount':
//                    $loan->update([
//                        'amount'=>$request->amount
//                    ]);
//                    break;
                case 'apply':
                    $terms_and_conditions=$this->generateTermsAndConditions($loan);
                    $loan->update([
                        'consent'=>$request->consent,
                        'terms_and_conditions'=>$terms_and_conditions,
                        'progress'=>1,
                    ]);
                    $data=[
                        'subject'       =>  'New Application',
                        'email'         => 'admin@zachanguloans.com',
                        'bodyMessage'   =>  "<div>Greetings!</div><br><br><div>$loan->first_name $loan->last_name has applied for a MK$loan->amount loan.</div><br>
<div>Visit <a href='https://app.zachanguloans.com/'>Zachangu Loans</a> to approve the loan.</div><br><br>
<div>Regards.</div>  ",
                    ];

                    $notification=$this->sendMail($data);

                    return response(['response'=>true,'notification'=>$notification,'loan'=>new Resources\LoanResource($loan)],200);
                    break;

                default:
                    break;
            }




        }
        return response(['response'=>false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $code)
    {
        $user=User::find($user_id);
        $loan=$user->loans()->where('code',$code)->first();

        if (is_object($loan)){
            $loan->delete();
            return response(['response'=>true],200);
        }
        return response(['response'=>false]);
    }


    public function uploadFile(Request $request)
    {

//        return response()->json([
//            'response'  =>  false,
//            'status'=>1
//        ]);

        $file_name='';
        if (isset($request->name)){
//            $name=str_slug($request->name);
            $name=$request->name;

            $file=$request->file;
            $type=$request->type;

            //upload new picture and update database
            $explodedFile=explode(',',$file);
            $decodedFile=base64_decode($explodedFile[1]);

            //develop name
            $ext=$this->getExtension($explodedFile);
            $filename="files/".$name."-".$type."-".uniqid().".".$ext;

            if($type=='photo'||$type=='photo_employee'){
                if($ext=='jpg' || $ext=='png'){
                    try{
                        Storage::disk('public_uploads')->put(
                            $filename,file_get_contents($file)
                        );
                    }catch (\RuntimeException $e){
                        return response()->json([
                            'response'  =>  false,
                            'status'=>4 //failed upload
                        ]);
                    }
                }else {
                    return response()->json([
                        'response' => false,
                        'status' => 5 //invalid extension for photo
                    ]);
                }
            } else{
                if($ext=='jpg' || $ext=='png' || $ext=='pdf'){
                    try{
                        Storage::disk('public_uploads')->put(
                            $filename,file_get_contents($file)
                        );
                    }catch (\RuntimeException $e){
                        return response()->json([
                            'response'  =>  false,
                            'status'=>4             //failed upload
                        ]);
                    }
                }else {
                    return response()->json([
                        'response' => false,
                        'status' => 6               //invalid extension for photo
                    ]);
                }
            }

            //for web configuration
            if($request->type=='agreement' || $request->type=='terms' || $request->type=='photo_employee')
                $filename='https://console.zachanguloans.com/'.$filename;
            else
                $filename='https://app.zachanguloans.com/'.$filename;

            return response()->json([
                'response'  =>  true,
                'new_file'      =>  $filename
            ]);
        }else{
            return response()->json([
                'response'  =>  false,
                'status'=>4
            ]);
        }
    }

    public static function getExtension($explodedImage)
    {
        $imageExtensionDecode=explode('/',$explodedImage[0]);
        $imageExtension=explode(';',$imageExtensionDecode[1]);
        $lowercaseExt=strtolower($imageExtension[0]);
        if($lowercaseExt=='jpeg')
            return 'jpg';
        else
            return $lowercaseExt;
    }

    public function exportPDF($code)
    {
        $loan=Loan::where('code',$code)->first();

        if (is_object($loan)){

//            $name=$loan->user->first_name.'-'.$loan->user->last_name;
//
//            $data=[
//                'date'                  => date('l, F d, Y'),
//                'account_number'        => $loan->bank_account_number,
//                'bank_name'             => $loan->bank_name,
//                'employee_name'         => $loan->user->first_name.' '.$loan->user->last_name,
//                'bank_service_center'   => $loan->bank_service_center,
//                'amount'                => $loan->amount,
//            ];
//
//            $pdf=PDF::loadView('pdf', $data);

//            $pdf->save('storage/consents/'.$name.'-consent-form-'.uniqid().'.pdf');

//            return response()->json([
//                'response'  =>  true,
//            ]);
//            return $pdf->download($name.'-consent-form-'.uniqid().'.pdf');

            $pdf=App::make('dompdf.wrapper');
            $pdf->loadHTML($loan->terms_and_conditions);
            return $pdf->stream();


        }else{
            return response()->json([
                'response'  =>  false,
                'status'=>4
            ]);
        }
    }

    public function getScore($loan){

        $employee=Employee::where('national_id_number',$loan->national_id_number)->first();

        //decoding physical address
        $employeePhysicalAddress=json_decode($employee->physical_address);
        $loanPhysicalAddress=json_decode($loan->physical_address);

        //calculating score
        similar_text($employeePhysicalAddress->name,$loanPhysicalAddress->name,$physicalAddressName);
        similar_text($employeePhysicalAddress->box,$loanPhysicalAddress->box,$physicalAddressBox);
        similar_text($employeePhysicalAddress->location,$loanPhysicalAddress->location,$physicalAddressLocation);

        //decoding work address
        $employeeWorkAddress=json_decode($employee->work_address);
        $loanWorkAddress=json_decode($loan->work_address);

        //calculating score
        similar_text($employeeWorkAddress->name,$loanWorkAddress->name,$workAddressName);
        similar_text($employeeWorkAddress->box,$loanWorkAddress->box,$workAddressBox);
        similar_text($employeeWorkAddress->location,$loanWorkAddress->location,$workAddressLocation);

        //computing dates
        $employeeContractDuration=date('jS F, Y',$employee->contract_duration);
        $loanContractDuration=date('jS F, Y',$loan->contract_duration);

        similar_text(substr($employeeContractDuration,0,-6),substr($loanContractDuration,0,-6),$contractDurationDate);


        similar_text($employee->first_name,$loan->first_name,$firstName);
        similar_text($employee->middle_name,$loan->middle_name,$middleName);
        similar_text($employee->last_name,$loan->last_name,$lastName);
        similar_text($employee->email,$loan->email,$email);
        similar_text($employee->phone_number_mobile,$loan->phone_number_mobile,$phoneNumberMobile);
        similar_text($employee->phone_number_work,$loan->phone_number_work,$phoneNumberWork);

        similar_text($employee->position,$loan->position,$position);

        //total score
        return (
                round($firstName)+
                round($middleName)+
                round($lastName)+
                round($email)+
                round($phoneNumberMobile)+
                round($phoneNumberWork)+
                round(($physicalAddressName + $physicalAddressBox + $physicalAddressLocation)/3)+
                round($position)+
                round((/*$workAddressName +*/ $workAddressBox + $workAddressLocation)/2)+
                round($contractDurationDate))/10;
    }

    public function exportDatasheet($datestamp)
    {
        $date=Carbon::create($datestamp)->toDateTimeString();
        $loans=Loan::where('created_at','>=',$date)->where('progress','>',0)->get();
        $loansData=[];
        $index=1;



        foreach ($loans as $loan){
            $status='';
            $dueDate=$loan->due_date!=null?date('jS F, Y',$loan->due_date):'N/A';
            $approvedDate=$loan->approved_date!=null?date('jS F, Y',$loan->approved_date):'N/A';
            $approvedTime=$loan->approved_date!=null?Carbon::createFromTimestamp($loan->approved_date,'Africa/Lusaka')->format('h:i A'):'N/A';
            $now=Carbon::now()->getTimestamp();

            $page=Page::first();
            $contents=json_decode($page->contents);

            if($loan->progress==1){
                $status='Approval pending';
            }
            elseif($loan->progress==2 && $now<$loan->due_date)
                $status='Active';
            elseif($loan->progress==2 && $now>$loan->due_date)
                $status='Due';
            elseif($loan->progress==3)
                $status='Closed';

            array_push($loansData,[
                'index'                 => $index,
                'firstName'             => $loan->first_name,
                'middleName'            => $loan->middle_name,
                'lastName'              => $loan->last_name,
                'phoneNumberMobile'     => '+265 '.$loan->phone_number_mobile,
                'phoneNumberWork'       => '+265 '.$loan->phone_number_work,
                'position'              => $loan->position,
                'email'                 => $loan->email,
                'nationalIdNumber'      => $loan->national_id_number,
                'loanAmount'            => $loan->amount,
                'amountDue'             => ($loan->amount)*(1+$contents->interest),
                'approvedTime'          => $approvedTime,
                'approvedDate'          => $approvedDate,
                'dueDate'               => $dueDate,
                'Status'               => $status,
                'employer'              => $loan->user->employer->name
            ]);
            $index++;
        }

//        dd($loansData);

        return Excel::download(new LoanExport($loansData),'loans.xlsx');
    }

    public static function sendMail($data){

        // Create the Transport
        $transport = (new Swift_SmtpTransport('mail.zachanguloans.com', 465,'ssl'))
            ->setUsername('admin@zachanguloans.com')
            ->setPassword('5E;dRxjC4:1Y9i')
        ;

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message='';
        if ($data['subject']=='Loan Approval'){
            $message = (new Swift_Message($data['subject']))
                ->setFrom('system@zachanguloans.com')
                ->setTo($data['email'])
                ->addCc('admin@zachanguloans.com')
                ->setBody($data['bodyMessage'],'text/html')
            ;
        }else{
            $message = (new Swift_Message($data['subject']))
                ->setFrom('system@zachanguloans.com')
                ->setTo($data['email'])
                ->setBody($data['bodyMessage'],'text/html')
            ;
        }


        // Send the message
        try{
            $result = $mailer->send($message);
            //if successful
            if($result==1)
                return true;
            else
                return false;
        }catch (\Swift_TransportException $exception){
            return false;
        }
    }

    public static function generateTermsAndConditions($loan)
    {
        $issuedDate=date('d/m/y');
        $day=date('d');
        $month=date('F');
        $year=date('Y');
        $fullname =$loan->user->first_name.' '.$loan->user->last_name;
        $decodedAddress=  json_decode($loan->physical_address);
        $address = $decodedAddress->name.' P. O. Box '.$decodedAddress->box.' '.$decodedAddress->location;

        $page=Page::first();
        $decodedContents=json_decode($page->contents);
        $interest=($decodedContents->interest)*100;


        return "<p><strong>ZACHANGU MICROFINANCE AGENCY</strong></p>

<p><strong>Payday Loan Agreement</strong></p>
<p><strong>Issued on: $issuedDate</strong></p>
<p>This Loan Agreement (this “Agreement”), is executed as of this <span>$day</span> day of <span>$month, $year</span> (the “Effective Date”)&nbsp;</p>
<p>By and between&nbsp;</p>
<p> <span>$fullname</span>, located at <span>$address</span> , hereinafter referred to as the “Borrower”;</p>
<p>And&nbsp;</p>
<p><strong>ZACHANGU MICROFINANCE AGENCY</strong>, located at <strong>MANJA TOWNSHIP</strong>, hereinafter referred to as the “Lender”;&nbsp;</p>
<p><strong>WHEREAS</strong> at the request of the Borrower, the Lender has agreed to grant a Payday Loan of <span>MK$loan->amount</span> to the Borrower till <span>$loan->due_date ??</span> on terms and conditions hereinafter contained.</p>
<p>The parties agree as follows:&nbsp;</p>
<p>1. <strong>Loan Amount</strong>: The Lender agrees to loan the Borrower the principal sum of <span>MK$loan->amount</span> (the “Loan”).&nbsp;</p>
<p>2. <strong>Interest:</strong> The payday loan bears interest at the rate of <span>$interest%</span> till the Borrower’s day of receiving salary: <span>$loan->pay_day</span></p>
<p>Calculations are as follows:&nbsp;</p>
<p>3. <strong>Repayment of Loan</strong>: The Loan, together with accrued and unpaid interest and all other charges, costs and expenses, is due and payable on or before <span>$loan->due_date ??</span>.&nbsp;</p>
<p>4. <strong>Penalty:</strong> Where the employer fails to honor the date of repayment for the loan, he/she will be given three days as grace period. If the Borrower fails to pay within the grace period of three days, after the three days, each day the borrower agrees to pay 10% of the due payment till payment is made. The Borrower will also pay to the Lender all charges the Lender met to collect the overdue payment.&nbsp;</p>
<p>5. <strong>Guarantee:</strong> Guarantor’s Full Name located at Guarantor’s Complete Address (the “Guarantor”) promises to unconditionally guarantee to the Lender, the full payment and performance by Borrower of all duties and obligations arising under this Agreement. The Guarantor agrees that this guarantee shall remain in full force and effect and be binding on the Guarantor until this Agreement is satisfied.&nbsp;</p>
<p>6. <strong>Prepayment:</strong> The Borrower has the right to prepay all or any part of the Loan, together with accrued and unpaid interest thereon, at any time without prepayment penalty or premium of any kind.&nbsp;</p>
<p>7. <strong>Costs and Expenses</strong>: Borrower shall pay to the Lender all costs of collection, including reasonable attorney's fees, the Lender incurs in enforcing this Agreement.&nbsp;</p>
<p>8. <strong>Waiver:</strong> The Borrower and all sureties, guarantors and endorsers here of, waive presentment, protest and demand, notice of protest, demand and dishonor and nonpayment of this Agreement.&nbsp;</p>
<p>9. <strong>Successors and Assigns</strong>: This Agreement will inure to the benefit of and be binding on the respective successors and permitted assigns of the Lender and the Borrower.&nbsp;</p>
<p>10. <strong>Amendment:</strong> This Agreement may be amended or modified only by a written agreement, duly signed by both the Borrower and the Lender.&nbsp;</p>
<p>11. <strong>Notices:</strong> Any notice or communication under this Loan must be in writing and sent via email.</p>
<p>12. <strong>No Waiver:</strong> Lender shall not be deemed to have waived any provision of this Agreement or the exercise of any rights held under this Agreement unless such waiver is made expressly and in writing. Waiver by Lender of a breach or violation of any provision of this Agreement shall not constitute a waiver of any other subsequent breach or violation.&nbsp;</p>
<p>13. <strong>Severability:</strong> In the event that any of the provisions of this Agreement are held to be invalid or unenforceable in whole or in part, the remaining provisions shall not be affected and shall continue to be valid and enforceable as though the invalid or unenforceable parts had not been included in this Agreement.&nbsp;</p>
<p>14. <strong>Assignment:</strong> Borrower shall not assign this Agreement, in whole or in part, without the written consent of Lender. Lender may assign all or any portion of this Agreement with written notice to Borrower.&nbsp;</p>
<p>15. <strong>Governing Law:</strong> This Agreement shall be governed by and construed in accordance with the laws of the Republic of Malawi, not including its conflicts of law provisions.&nbsp;</p>
<p>16. <strong>Disputes:</strong> Any dispute arising from this Agreement shall be resolved in the courts of the Republic of Malawi.&nbsp;</p>
<p>17. <strong>Entire Agreement:</strong> This Agreement contains the entire understanding between the parties and supersedes and cancels all prior agreements of the parties, whether oral or written, with respect to such subject matter.&nbsp;</p>
<p>IN WITNESS WHEREOF, the parties have executed this Agreement as of the date first stated above.</p>"
;
    }
}
