<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//User Controller
Route::group(['prefix' => 'pages'], function() {
    Route::get('/', 'PageController@index');

    Route::group([
        'middleware' => ['jwt.verify']
    ], function () {
        Route::post('/', 'PageController@store');
    });

});//User Controller
Route::group(['prefix' => 'users'], function() {
    Route::post('/', 'UserController@store');
    Route::post('/login/attempt', 'UserController@login');
    Route::post('/feedback/send', 'UserController@feedback');

    Route::group([
        'middleware' => ['jwt.verify']
    ], function () {
        Route::get('/', 'UserController@index');
        Route::get('/{id}/eligibility', 'UserController@eligibility');
        Route::get('/{id}', 'UserController@show');
        Route::post('/{id}', 'UserController@update');
        Route::delete('/{id}', 'UserController@destroy');
    });

});

//Employer Controller
Route::group(['prefix' => 'employers'], function() {
    Route::get('/', 'EmployerController@index');

    Route::group([
        'middleware' => ['jwt.verify']
    ], function () {
        Route::get('/{id}', 'EmployerController@show');
        Route::post('/', 'EmployerController@store');
        Route::post('/{id}', 'EmployerController@update');
        Route::delete('/{id}', 'EmployerController@destroy');
    });
});

//Employee Controller
Route::group([
    'prefix' => 'employees',
    'middleware' => ['jwt.verify']
], function() {
    Route::get('/', 'EmployeeController@index');
    Route::get('/{id}', 'EmployeeController@show');
    Route::post('/', 'EmployeeController@store');
    Route::post('/{id}', 'EmployeeController@update');
    Route::delete('/{id}', 'EmployeeController@destroy');
});

//Loans Controller
Route::group([
    'prefix' => 'loans',
    'middleware' => ['jwt.verify']
], function() {
    Route::get('/{user_id}/', 'LoanController@index');
    Route::get('/{user_id}/{code}/{type}/get', 'LoanController@show');
    Route::get('/export/pdf/{code}', 'LoanController@exportPDF');
    Route::get('/export/datasheet/{datestamp}', 'LoanController@exportDatasheet');
    Route::post('/upload/file', 'LoanController@uploadFile');
    Route::post('/', 'LoanController@store');
    Route::post('/subscribed', 'LoanController@storeSubscribed');
    Route::post('/{user_id}/{code}', 'LoanController@update');
    Route::delete('/{user_id}/{code}', 'LoanController@destroy');
});

//Admin Routes
Route::group([
    'prefix' => 'admin',
    'middleware' => ['jwt.verify']
], function() {
    Route::get('/loans/', 'LoanController@indexAdmin');
    Route::get('/loans/{user_id}', 'LoanController@indexUserAdmin');
    Route::get('/loans/{code}/{type}', 'LoanController@modify');
//    Route::get('/{user_id}/{code}/{type}/get', 'LoanController@show');
//    Route::get('/export/pdf/{code}', 'LoanController@exportPDF');
//    Route::post('/upload/file', 'LoanController@uploadFile');
//    Route::post('/', 'LoanController@store');
//    Route::post('/{user_id}/{code}', 'LoanController@update');
//    Route::delete('/{user_id}/{code}', 'LoanController@destroy');
});