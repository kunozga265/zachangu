<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Zachangu Consent Form</title>
    <style>
        .fields{
            width:220px;
            display: inline-block;
        }
        .field{
            width: 100%;
            border: 2px solid black;
            height:60px;
        }
    </style>
</head>
<body>
    <p>{{$date}}</p>

    <br>
    <p>The Principle Officer</p>
    <p>Zachangu Microfinance Agency</p>
    <p>P O Box 3053</p>
    <p>Blantyre</p>

    <br>
    <p>Dear Sir</p>

    <h4>IRREVOCABLE LETTER OF UNDERTAKING TO SEND SALARY TO ACCOUNT NUMBER <strong><strong>{{$account_number}}</strong></strong></h4>

    <p>We have been informed by <strong>{{$employee_name}}</strong>, that he/she is requesting for a loan facility of <strong>MK{{$amount}}</strong> only from Zachangu Microfinance Agency.</p>

    <p>The purpose of this letter is to confirm that the above named is our employee and to make certain undertakings regarding payments of the sought facility when granted.</p>

    <p>Based on the authorization and declaration from the employee that we have on file, we will continue to send his salary to his account number <strong>{{$account_number}}</strong> held at <strong>{{$bank_service_center}}</strong> service center until directed otherwise in writing by yourselves.</p>

    <p>We confirm that no similar undertaking has been given to other financial institutions and that no similar one will be issued to other finance providers without confirming with <strong>{{$bank_name}}</strong>.</p>

    <p>We further undertake to immediately inform the bank of any changes in the status of his employment to enable the bank take appropriate action.</p>

    <br>
    <p>Yours faithfully,</p>

    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="fields">
        <h4>NAME</h4>
        <div class="field">

        </div>
    </div>
    <div class="fields">
        <h4>POSITION</h4>
        <div class="field">

        </div>
    </div>

    <div class="fields">
        <h4>NATIONAL ID</h4>
        <div class="field">

        </div>
    </div>



</body>
</html>

