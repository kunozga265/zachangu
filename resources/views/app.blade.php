<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<link href="{{asset('/css/material-design-icons/iconfont/material-icons.css')}}" rel="stylesheet" type="text/css"/>--}}

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
    <link href="{{asset('/css/app.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/x-icon" href="{{asset('/favicon.png')}}">

    <title>Zachangu Loans</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <script type='text/javascript'>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="app"><router-view></router-view></div>
<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
</body>
</html>
