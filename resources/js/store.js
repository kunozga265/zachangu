/*
  Adds the promise polyfill for IE 11
*/
require('es6-promise').polyfill();

/*
    Imports Vue and Vuex
*/
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

/*
    Initializes Vuex on Vue.
*/
Vue.use( Vuex );

//Sets up Vuex Persist
const vuexLocalStorage=new VuexPersist({
    key:        'vuex', //The key to store the state on in the storage provider.
    storage:    window.localStorage, //or window.sessionStorage or localForage

    //Function that passes the state and returns the state with only the objects you want to store
    // reducer:state=>state,
    reducer:state=>({
        session:state.session,
    }),
    //Function that passes a mutation and lets you decide if it should update the state in localStorage
    // filter:   mutation=> (true)
    filter:(mutation)=> mutation.type=='setSession'
});

/*


/*
    Imports all of the modules used in the application to build the data store.
*/
import { employees } from './modules/APIEmployeeModule.js'
import { employers } from './modules/APIEmployerModule.js'
import { users } from './modules/APIUserModule.js'
import { loans } from './modules/APILoanModule.js'
import { contents } from './modules/APIContentModule.js'
import { session } from './modules/SessionModule.js'


/*
  Exports our data store.
*/
export default new Vuex.Store({
    modules: {
        employees,
        employers,
        users,
        loans,
        contents,
        session,
    },
    plugins:[vuexLocalStorage.plugin]
});