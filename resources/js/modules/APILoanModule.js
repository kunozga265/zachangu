/*
|-------------------------------------------------------------------------------
| VUEX modules/Loan.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Loans
*/

import API from '../api/APILoanController.js';

export const loans = {
    state: {
        Loans:[],
        LoansLoadStatus:0,
        LoansStoreStatus:0,
        LoansUpdateStatus:0,
        LoansDeleteStatus:0,

        Loan:{},
        LoanData:{},
        LoanLoadStatus:0,

        lastPage:0,
        currentPage:0,

        UploadedFile:'',
        UploadStatus:'',
    },
    actions:{
        indexLoanAdmin({commit},data){
            commit('setLoansLoadStatus',1);

            if(data.accessToken!==null){
                API.indexAdmin(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansLoadStatus',4);
                        }
                        else{
                            commit('setLoansLoadStatus',2);
                            commit('setLoans',response.data.loans);
                        }
                    })
                    .catch(function () {
                        commit('setLoansLoadStatus',3);
                    });
            }
        },
        indexLoanUserAdmin({commit},data){
            commit('setLoansLoadStatus',1);
            let page=1;

            if(data.accessToken!==null){
                API.indexUserAdmin(data,page)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansLoadStatus',4);
                        }
                        else{
                            commit('setLoansLoadStatus',2);
                            commit('setLoans',response.data.loans.data);
                            commit('setLastPage',response.data.loans.meta.last_page);
                            commit('setCurrentPage',response.data.loans.meta.current_page);
                        }
                    })
                    .catch(function () {
                        commit('setLoansLoadStatus',3);
                    });
            }
        },
        indexLoanUserAdminAppend({commit,state},data){
            commit('setLoansLoadStatus',1);

            if(data.accessToken!==null && state.currentPage<state.lastPage){
                let page=state.currentPage+1;
                API.indexUserAdmin(data,page)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansLoadStatus',4);
                        }
                        else{
                            commit('setLoansLoadStatus',2);
                            commit('appendLoans',response.data.loans.data);
                            commit('setLastPage',response.data.loans.meta.last_page);
                            commit('setCurrentPage',response.data.loans.meta.current_page);
                        }
                    })
                    .catch(function () {
                        commit('setLoansLoadStatus',3);
                    });
            }
        },
        indexLoan({commit},data){
            commit('setLoansLoadStatus',1);

            if(data.accessToken!==null){
                API.index(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansLoadStatus',4);
                        }
                        else{
                            commit('setLoansLoadStatus',2);
                            commit('setLoans',response.data.loans);
                        }
                    })
                    .catch(function () {
                        commit('setLoansLoadStatus',3);
                    });
            }
        },
        showLoan({commit,state},data){
            commit('setLoanLoadStatus',1);

            if(data.accessToken!==null){
                API.show(data)
                    .then(function (response) {
                        if (response.data.response == false) {
                            commit('setLoanLoadStatus', 4);
                        }
                        else{
                            commit('setLoanLoadStatus', 2);
                            commit('setLoan', response.data.loan);
                        }
                    })
                    .catch(function () {
                        commit('setLoan', {});
                        commit('setLoanLoadStatus', 3);
                    })
            }
        },

        storeSubscribedLoan({commit},data){
            commit('setLoansStoreStatus',1);

            if(data.accessToken!==null){
                API.storeSubscribed(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansStoreStatus',4);
                        }
                        else{
                            commit('setLoansStoreStatus',2);
                            commit('setLoanData',response.data.loanData);
                        }
                    })
                    .catch(function (response) {
                        commit('setLoansStoreStatus',response);
                    })
            }
        },
        storeLoan({commit},data){
            commit('setLoansStoreStatus',1);

            if(data.accessToken!==null){
                API.store(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansStoreStatus',4);
                        }
                        else{
                            commit('setLoansStoreStatus',2);
                            commit('setLoanData',response.data.loanData);
                        }
                    })
                    .catch(function (response) {
                        commit('setLoansStoreStatus',response);
                    })
            }
        },
        modifyLoan({commit},data){
            commit('setLoansUpdateStatus',1);

            if(data.accessToken!==null){
                API.modify(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansUpdateStatus',4);
                        }
                        else{
                            commit('setLoansUpdateStatus',2);
                        }
                    })
                    .catch(function () {
                        commit('setLoansUpdateStatus',3);
                    })
            }
        },
        updateLoan({commit},data){
            commit('setLoansUpdateStatus',1);

            if(data.accessToken!==null){
                API.update(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansUpdateStatus',4);
                        }
                        else{
                            commit('setLoansUpdateStatus',2);
                            if(data.type==='apply')
                                commit('setLoan',response.data.loan);
                            else
                                commit('setLoanData',response.data.loanData);
                        }
                    })
                    .catch(function () {
                        commit('setLoansUpdateStatus',3);
                    })
            }
        },
        destroyLoan({commit},data){
            commit('setLoansDeleteStatus',1);

            if(data.accessToken!==null){
                API.destroy(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setLoansDeleteStatus',4);
                        }
                        else{
                            commit('setLoansDeleteStatus',2);
                        }
                    })
                    .catch(function () {
                        commit('setLoansDeleteStatus',3);
                    })
            }
        },
        uploadFile({commit},data){
            commit('setUploadStatus',1);
            API.upload(data)
                .then(function (response) {
                    if(response.data.response==false){
                        commit('setUploadStatus',response.data.status);
                    }
                    else{
                        commit('setUploadStatus',2);
                        commit('setUploadedFile',response.data.new_file);
                    }
                })
                .catch(function () {
                    commit('setUploadStatus',3);
                })
        },
        exportPDF({commit},data){
            // commit('setUploadStatus',1);
            API.exportPDF(data)
                .then(function (response) {
                    if(response.data.response==false){
                        // commit('setUploadStatus',response.data.status);
                    }
                    else{
                        // commit('setUploadStatus',2);
                        const filename='Zachangu_license_agreement.pdf';
                        const url=window.URL.createObjectURL(new Blob([response.data]));
                        const link=document.createElement('a');
                        link.href=url;
                        link.setAttribute('download',filename);
                        document.body.appendChild(link);
                        link.click();

                    }
                })
                .catch(function () {
                    // commit('setUploadStatus',3);
                })
        },
        exportDatasheet({commit},data){
            // commit('setUploadStatus',1);
            API.exportDatasheet(data)
                .then(function (response) {
                    if(response.data.response==false){
                        // commit('setUploadStatus',response.data.status);
                    }
                    else{
                        // commit('setUploadStatus',2);
                        const filename='Loans Datasheet.xlsx';
                        const url=window.URL.createObjectURL(new Blob([response.data]));
                        const link=document.createElement('a');
                        link.href=url;
                        link.setAttribute('download',filename);
                        document.body.appendChild(link);
                        link.click();

                    }
                })
                .catch(function () {
                    // commit('setUploadStatus',3);
                })
        },
    },
    mutations:{
        //Loans
        setLoans(state,Loans){
            state.Loans=Loans;
        },
        appendLoans(state,Loans){
            for(let x in Loans){
                (state.Loans).push(Loans[x]);
            }
        },
        setLoansLoadStatus(state,status){
            state.LoansLoadStatus=status;
        },
        //Loan
        setLoanData(state,LoanData){
            state.LoanData=LoanData;
        },
        setLoan(state,Loan){
            state.Loan=Loan;
        },
        setLoanLoadStatus(state,status){
            state.LoanLoadStatus=status;
        },
        setLoansStoreStatus(state,status){
            state.LoansStoreStatus=status;
        },
        setLoansUpdateStatus(state,status){
            state.LoansUpdateStatus=status;
        },
        setLoansDeleteStatus(state,status){
            state.LoansDeleteStatus=status;
        },
        //Pagination
        setLastPage(state,lastPage){
            state.lastPage=lastPage;
        },
        setCurrentPage(state,currentPage){
            state.currentPage=currentPage;
        },
        //uploading files
        setUploadStatus(state,status){
            state.UploadStatus=status;
        },
        setUploadedFile(state,file){
            state.UploadedFile=file;
        },

    },
    getters:{
        getLoans(state){
            return state.Loans;
        },
        getLoansLoadStatus(state){
            return state.LoansLoadStatus;
        },
        getLoansStoreStatus(state){
            return state.LoansStoreStatus;
        },
        getLoansUpdateStatus(state){
            return state.LoansUpdateStatus;
        },
        getLoansDeleteStatus(state){
            return state.LoansDeleteStatus;
        },
        getLoan(state){
            return state.Loan;
        },
        getLoanData(state){
            return state.LoanData;
        },
        getLoanLoadStatus(state){
            return state.LoanLoadStatus;
        },
        getLoanDeleteStatus(state){
            return state.LoanDeleteStatus;
        },
        getUploadedFile(state){
            return state.UploadedFile;
        },
        getUploadStatus(state){
            return state.UploadStatus;
        },
        getLoansMorePages(state){
            return state.currentPage<state.lastPage
        },
    }

};