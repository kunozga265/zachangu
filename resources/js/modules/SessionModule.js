/*
|-------------------------------------------------------------------------------
| VUEX modules/Session.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Sessions
*/

export const session = {
    state: {
        Session:{
            'status':false
        },
     
    },
    actions:{
        storeSession({commit},data){
            commit('setSession',data);
        },
    },
    mutations:{
        setSession(state,Session){
            state.Session=Session;
        },
    },
    getters:{
        getSession(state){
            return state.Session;
        },
        getSessionStatus(state){
            return state.Session.status;
        },
    }

};