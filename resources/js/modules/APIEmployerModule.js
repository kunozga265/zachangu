/*
|-------------------------------------------------------------------------------
| VUEX modules/Employer.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Employers
*/

import API from '../api/APIEmployerController.js';

export const employers = {
    state: {
        Employers:[],
        EmployersLoadStatus:0,
        EmployersStoreStatus:0,
        EmployersUpdateStatus:0,
        EmployersDeleteStatus:0,

        EmployerData:'',
        Employer:{},
        EmployerLoadStatus:0,

        lastPage:0,
    },
    actions:{
        indexEmployer({commit},data){
            commit('setEmployersLoadStatus',1);

            API.index()
                .then(function (response) {
                    if(response.data.response==false){
                        commit('setEmployersLoadStatus',4);
                    }
                    else{
                        commit('setEmployersLoadStatus',2);
                        commit('setEmployers',response.data.employers);
                    }

                })
                .catch(function () {
                    commit('setEmployers',[]);
                    commit('setEmployersLoadStatus',3);
                });

        },
        selectedEmployer({commit,state},data){
            commit('setEmployer', data.employer);
            commit('setEmployerLoadStatus', 2);
        },
        showEmployer({commit,state},data){
            commit('setEmployerLoadStatus',1);

            if(data.accessToken!==null){
                API.show(data)
                    .then(function (response) {
                        if (response.data.response == false) {
                            commit('setEmployerLoadStatus', 4);
                        }
                        else{
                            commit('setEmployer', response.data);
                            commit('setEmployerLoadStatus', 2);
                        }
                    })
                    .catch(function () {
                        commit('setEmployer', {});
                        commit('setEmployerLoadStatus', 3);
                    })
            }
        },

        storeEmployer({commit},data){
            commit('setEmployersStoreStatus',1);

            if(data.accessToken!==null){
                API.store(data)
                    .then(function (response) {
                        // console.log(response);
                        if(response.data.response==false){
                            commit('setEmployersStoreStatus',4);
                        }
                        else{
                            commit('setEmployersStoreStatus',2);

                            commit('setEmployerData',response.data.employerData);
                        }
                    })
                    .catch(function () {
                        commit('setEmployersStoreStatus',3);
                    })
            }
        },
        updateEmployer({commit},data){
            commit('setEmployersUpdateStatus',1);

            if(data.accessToken!==null){
                API.update(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setEmployersUpdateStatus',4);
                        }
                        else{
                            commit('setEmployersUpdateStatus',2);
                            commit('setEmployerData',response.data.employerData);
                        }
                    })
                    .catch(function () {
                        commit('setEmployersUpdateStatus',3);
                    })
            }
        },
        destroyEmployer({commit},data){
            commit('setEmployersDeleteStatus',1);

            if(data.accessToken!==null){
                API.destroy(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setEmployersDeleteStatus',4);
                        }
                        else{
                            commit('setEmployersDeleteStatus',2);
                            commit('setEmployers',response.data.employers);
                        }
                    })
                    .catch(function () {
                        commit('setEmployersDeleteStatus',3);
                    })
            }
        },
    },
    mutations:{
        //Employers
        setEmployers(state,Employers){
            state.Employers=Employers;
        },
        setEmployersLoadStatus(state,status){
            state.EmployersLoadStatus=status;
        },
        //Employer
        setEmployer(state,Employer){
            state.Employer=Employer;
        },
        setEmployerData(state,EmployerData){
            state.EmployerData=EmployerData;
        },
        setEmployerLoadStatus(state,status){
            state.EmployerLoadStatus=status;
        },
        setEmployersStoreStatus(state,status){
            state.EmployersStoreStatus=status;
        },
        setEmployersUpdateStatus(state,status){
            state.EmployersUpdateStatus=status;
        },
        setEmployersDeleteStatus(state,status){
            state.EmployersDeleteStatus=status;
        },
        //Pagination
        setLastPage(state,lastPage){
            state.lastPage=lastPage;
        }

    },
    getters:{
        getEmployers(state){
            return state.Employers;
        },
        getEmployersLoadStatus(state){
            return state.EmployersLoadStatus;
        },
        getEmployersStoreStatus(state){
            return state.EmployersStoreStatus;
        },
        getEmployersUpdateStatus(state){
            return state.EmployersUpdateStatus;
        },
        getEmployersDeleteStatus(state){
            return state.EmployersDeleteStatus;
        },
        getEmployer(state){
            return state.Employer;
        },
        getEmployerData(state){
            return state.EmployerData;
        },
        getEmployerLoadStatus(state){
            return state.EmployerLoadStatus;
        },
        getEmployerDeleteStatus(state){
            return state.EmployerDeleteStatus;
        },

    }

};