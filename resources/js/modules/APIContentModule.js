/*
|-------------------------------------------------------------------------------
| VUEX modules/Content.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Contents
*/

import API from '../api/APIContentController.js';

export const contents = {
    state: {
        Contents:[],
        ContentsLoadStatus:0,
        ContentsStoreStatus:0,
        ContentsUpdateStatus:0,
        ContentsDeleteStatus:0,

        ContentData:'',
        Content:{},
        ContentLoadStatus:0,

        lastContent:0,
    },
    actions:{
        indexContent({commit},data){
            commit('setContentsLoadStatus',1);

            API.index()
                .then(function (response) {
                    if(response.data.response==false){
                        commit('setContentsLoadStatus',4);
                    }
                    else{
                        commit('setContentsLoadStatus',2);
                        commit('setContents',response.data.contents);
                    }

                })
                .catch(function () {
                    commit('setContents',[]);
                    commit('setContentsLoadStatus',3);
                });

        },
       
        storeContent({commit},data){
            commit('setContentsStoreStatus',1);

            if(data.accessToken!==null){
                API.store(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setContentsStoreStatus',4);
                        }
                        else{
                            commit('setContentsStoreStatus',2);
                            commit('setContents',response.data.contents);
                        }
                    })
                    .catch(function () {
                        commit('setContentsStoreStatus',3);
                    })
            }
        },
    },
    mutations:{
        //Contents
        setContents(state,Contents){
            state.Contents=Contents;
        },
        setContentsLoadStatus(state,status){
            state.ContentsLoadStatus=status;
        },
        //Content
        setContent(state,Content){
            state.Content=Content;
        },
        setContentData(state,ContentData){
            state.ContentData=ContentData;
        },
        setContentLoadStatus(state,status){
            state.ContentLoadStatus=status;
        },
        setContentsStoreStatus(state,status){
            state.ContentsStoreStatus=status;
        },
        setContentsUpdateStatus(state,status){
            state.ContentsUpdateStatus=status;
        },
        setContentsDeleteStatus(state,status){
            state.ContentsDeleteStatus=status;
        },
        //Pagination
        setLastContent(state,lastContent){
            state.lastContent=lastContent;
        }

    },
    getters:{
        getContents(state){
            return state.Contents;
        },
        getContentsLoadStatus(state){
            return state.ContentsLoadStatus;
        },
        getContentsStoreStatus(state){
            return state.ContentsStoreStatus;
        },
        getContentsUpdateStatus(state){
            return state.ContentsUpdateStatus;
        },
        getContentsDeleteStatus(state){
            return state.ContentsDeleteStatus;
        },
        getContent(state){
            return state.Content;
        },
        getContentData(state){
            return state.ContentData;
        },
        getContentLoadStatus(state){
            return state.ContentLoadStatus;
        },
        getContentDeleteStatus(state){
            return state.ContentDeleteStatus;
        },

    }

};