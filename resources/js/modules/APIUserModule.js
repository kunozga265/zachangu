/*
|-------------------------------------------------------------------------------
| VUEX modules/User.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Users
*/

import API from '../api/APIUserController.js';

export const users = {
    state: {
        Users:[],
        UsersLoadStatus:0,
        UsersStoreStatus:0,
        UsersUpdateStatus:0,
        UsersDeleteStatus:0,

        User:{},
        currentUser:{},
        UserLoadStatus:0,
        UserLoginStatus:0,

        lastPage:0,
        currentPage:0,

        Eligibility:[],
        EligibilityStatus:0,

        FeedbackStatus:0
    },
    actions:{
        userEligibility({commit},data){
            commit('setEligibilityStatus',1);

            if(data.accessToken!==null){
                API.eligibility(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setEligibilityStatus',4);
                            commit('setEligibility',false);
                        }
                        else{
                            commit('setEligibilityStatus',2);
                            commit('setEligibility',response.data.eligibility);
                        }
                    })
                    .catch(function () {
                        commit('setEligibilityStatus',3);
                    });
            }
        },
        indexUser({commit},data){
            commit('setUsersLoadStatus',1);
            let page=1;

            if(data.accessToken!==null){
                API.index(data,page)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUsersLoadStatus',4);
                        }
                        else{
                            commit('setUsersLoadStatus',2);
                            commit('setUsers',response.data.users.data);
                            commit('setLastPage',response.data.users.meta.last_page);
                            commit('setCurrentPage',response.data.users.meta.current_page);
                        }

                    })
                    .catch(function () {
                        commit('setUsersLoadStatus',3);
                    });
            }
        },
        appendUser({commit,state},data){
            commit('setUsersLoadStatus',1);

            if(data.accessToken!==null && state.currentPage<state.lastPage){
                let page=state.currentPage+1;
                API.index(data,page)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUsersLoadStatus',4);
                        }
                        else{
                            commit('setUsersLoadStatus',2);
                            commit('appendUsers',response.data.users.data);
                            commit('setLastPage',response.data.users.meta.last_page);
                            commit('setCurrentPage',response.data.users.meta.current_page);
                        }

                    })
                    .catch(function () {
                        commit('setUsersLoadStatus',3);
                    });
            }
        },
        showUser({commit,state},data){
            commit('setUserLoadStatus',1);

            if(data.accessToken!==null){
                API.show(data)
                    .then(function (response) {
                        if (response.data.response == false) {
                            commit('setUserLoadStatus', 4);
                        }
                        else{
                            commit('setCurrentUser', response.data.user);
                            commit('setUserLoadStatus', 2);
                        }
                    })
                    .catch(function () {
                        commit('setUser', {});
                        commit('setUserLoadStatus', 3);
                    })
            }
        },

        registerUser({commit},data){
            commit('setUserLoginStatus',1);
            API.store(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUserLoginStatus',4);
                        }
                        else{
                            commit('setUserLoginStatus',2);
                            commit('setUser',response.data.user);
                        }
                    })
                    .catch(function () {
                        commit('setUserLoginStatus',3);
                    })
        },

        storeUser({commit},data){
            commit('setUsersStoreStatus',1);
            API.store(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUsersStoreStatus',4);
                        }
                        else{
                            commit('setUsersStoreStatus',2);
                            commit('setUser',response.data.user);
                        }
                    })
                    .catch(function () {
                        commit('setUsersStoreStatus',3);
                    })
        },

        login({commit},data){
            commit('setUserLoginStatus',1);
            API.login(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUserLoginStatus',response.data.status);
                        }
                        else{
                            commit('setUserLoginStatus',2);
                            commit('setUser',response.data.user);
                        }
                    })
                    .catch(function () {
                        commit('setUserLoginStatus',3);
                    })
        },
        forceLogin({commit,state},data){
            commit('setUser', data.user);
            commit('setUserLoginStatus', 2);
        },
        logoutUser({commit,state},data){
            commit('setUserLoginStatus',5);
            commit('setUser',{});
        },

        updateUser({commit},data){
            commit('setUsersUpdateStatus',1);

            if(data.accessToken!==null){
                API.update(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUsersUpdateStatus',response.data.status);
                        }
                        else{
                            commit('setUsersUpdateStatus',2);
                            commit('setUser',response.data.user);
                        }
                    })
                    .catch(function () {
                        commit('setUsersUpdateStatus',3);
                    })
            }
        },
        destroyUser({commit},data){
            commit('setUsersDeleteStatus',1);

            if(data.accessToken!==null){
                API.destroy(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setUsersDeleteStatus',4);
                        }
                        else{
                            commit('setUsersDeleteStatus',2);
                            commit('setUsers',response.data.users);
                        }
                    })
                    .catch(function () {
                        commit('setUsersDeleteStatus',3);
                    })
            }
        },
        feedback({commit},data){
            commit('setFeedbackStatus',1);

            API.feedback(data)
                .then(function (response) {
                    if(response.data.response==false){
                        commit('setFeedbackStatus',response.data.status);
                    }
                    else{
                        commit('setFeedbackStatus',2);
                    }
                })
                .catch(function () {
                    commit('setFeedbackStatus',3);
                })

        },
    },
    mutations:{
        //Users
        setUsers(state,Users){
            state.Users=Users;
        },
        appendUsers(state,Users){
            for(let x in Users){
                (state.Users).push(Users[x]);
            }
        },
        setUsersLoadStatus(state,status){
            state.UsersLoadStatus=status;
        },
        //User
        setUser(state,User){
            state.User=User;
        },
        setCurrentUser(state,User){
            state.currentUser=User;
        },
        setUserLoadStatus(state,status){
            state.UserLoadStatus=status;
        },
        setUserLoginStatus(state,status){
            state.UserLoginStatus=status;
        },
        setUsersStoreStatus(state,status){
            state.UsersStoreStatus=status;
        },
        setUsersUpdateStatus(state,status){
            state.UsersUpdateStatus=status;
        },
        setUsersDeleteStatus(state,status){
            state.UsersDeleteStatus=status;
        },
        //Pagination
        setLastPage(state,lastPage){
            state.lastPage=lastPage;
        },
        setCurrentPage(state,currentPage){
            state.currentPage=currentPage;
        },
        setEligibility(state,Eligibility){
            state.Eligibility=Eligibility;
        },
        setEligibilityStatus(state,status){
            state.EligibilityStatus=status;
        },
        setFeedbackStatus(state,status){
            state.FeedbackStatus=status;
        }

    },
    getters:{
        getUserId(state){
            return state.User.id;
        },
        getUserEligibility(state){
            return state.Eligibility.status;
        },
        getEligibility(state){
            return state.Eligibility;
        },
        getEligibilityStatus(state){
            return state.EligibilityStatus;
        },
        getAccessToken(state){
            return state.User.accessToken;
        },
        getUsers(state){
            return state.Users;
        },
        getUsersLoadStatus(state){
            return state.UsersLoadStatus;
        },
        getUserLoginStatus(state){
            return state.UserLoginStatus;
        },
        getUsersStoreStatus(state){
            return state.UsersStoreStatus;
        },
        getUsersUpdateStatus(state){
            return state.UsersUpdateStatus;
        },
        getUsersDeleteStatus(state){
            return state.UsersDeleteStatus;
        },
        getUser(state){
            return state.User;
        },
        getCurrentUser(state){
            return state.currentUser;
        },
        getUserLoadStatus(state){
            return state.UserLoadStatus;
        },
        getUserDeleteStatus(state){
            return state.UserDeleteStatus;
        },
        getMorePages(state){
            return state.currentPage<state.lastPage
        },
        getAdmin(state){
            return state.User.admin
        },
        getFeedbackStatus(state){
            return state.FeedbackStatus;
        },
    }

};