/*
|-------------------------------------------------------------------------------
| VUEX modules/Employee.js
|-------------------------------------------------------------------------------
| The Vuex data store for the Employees
*/

import API from '../api/APIEmployeeController.js';

export const employees = {
    state: {
        Employees:[],
        EmployeesLoadStatus:0,
        EmployeesStoreStatus:0,
        EmployeesUpdateStatus:0,
        EmployeesDeleteStatus:0,

        EmployeeData:'',
        Employee:{},
        EmployeeLoadStatus:0,

        lastPage:0,
    },
    actions:{
        indexEmployee({commit},data){
            commit('setEmployeesLoadStatus',1);

            API.index()
                .then(function (response) {
                    if(response.data.response==false){
                        commit('setEmployeesLoadStatus',4);
                    }
                    else{
                        commit('setEmployeesLoadStatus',2);
                        commit('setEmployees',response.data.Employees);
                    }

                })
                .catch(function () {
                    commit('setEmployees',[]);
                    commit('setEmployeesLoadStatus',3);
                });

        },
        selectedEmployee({commit,state},data){
            commit('setEmployee', data.Employee);
            commit('setEmployeeLoadStatus', 2);
        },
        showEmployee({commit,state},data){
            commit('setEmployeeLoadStatus',1);

            if(data.accessToken!==null){
                API.show(data)
                    .then(function (response) {
                        if (response.data.response == false) {
                            commit('setEmployeeLoadStatus', 4);
                        }
                        else{
                            commit('setEmployee', response.data.employee);
                            commit('setEmployeeLoadStatus', 2);
                        }
                    })
                    .catch(function () {
                        commit('setEmployee', {});
                        commit('setEmployeeLoadStatus', 3);
                    })
            }
        },

        storeEmployee({commit},data){
            commit('setEmployeesStoreStatus',1);

            if(data.accessToken!==null){
                API.store(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setEmployeesStoreStatus',4);
                        }
                        else{
                            commit('setEmployeesStoreStatus',2);
                            commit('setEmployeeData',response.data.employeeData);
                        }
                    })
                    .catch(function () {
                        commit('setEmployeesStoreStatus',3);
                    })
            }
        },
        updateEmployee({commit},data){
            commit('setEmployeesUpdateStatus',1);

            if(data.accessToken!==null){
                API.update(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setEmployeesUpdateStatus',4);
                        }
                        else{
                            commit('setEmployeesUpdateStatus',2);
                            commit('setEmployeeData',response.data.employeeData);
                        }
                    })
                    .catch(function () {
                        commit('setEmployeesUpdateStatus',3);
                    })
            }
        },
        destroyEmployee({commit},data){
            commit('setEmployeesDeleteStatus',1);

            if(data.accessToken!==null){
                API.destroy(data)
                    .then(function (response) {
                        if(response.data.response==false){
                            commit('setEmployeesDeleteStatus',4);
                        }
                        else{
                            commit('setEmployeesDeleteStatus',2);
                            commit('setEmployees',response.data.Employees);
                        }
                    })
                    .catch(function () {
                        commit('setEmployeesDeleteStatus',3);
                    })
            }
        },
    },
    mutations:{
        //Employees
        setEmployees(state,Employees){
            state.Employees=Employees;
        },
        setEmployeesLoadStatus(state,status){
            state.EmployeesLoadStatus=status;
        },
        //Employee
        setEmployee(state,Employee){
            state.Employee=Employee;
        },
        setEmployeeData(state,EmployeeData){
            state.EmployeeData=EmployeeData;
        },
        setEmployeeLoadStatus(state,status){
            state.EmployeeLoadStatus=status;
        },
        setEmployeesStoreStatus(state,status){
            state.EmployeesStoreStatus=status;
        },
        setEmployeesUpdateStatus(state,status){
            state.EmployeesUpdateStatus=status;
        },
        setEmployeesDeleteStatus(state,status){
            state.EmployeesDeleteStatus=status;
        },
        //Pagination
        setLastPage(state,lastPage){
            state.lastPage=lastPage;
        }

    },
    getters:{
        getEmployees(state){
            return state.Employees;
        },
        getEmployeesLoadStatus(state){
            return state.EmployeesLoadStatus;
        },
        getEmployeesStoreStatus(state){
            return state.EmployeesStoreStatus;
        },
        getEmployeesUpdateStatus(state){
            return state.EmployeesUpdateStatus;
        },
        getEmployeesDeleteStatus(state){
            return state.EmployeesDeleteStatus;
        },
        getEmployee(state){
            return state.Employee;
        },
        getEmployeeData(state){
            return state.EmployeeData;
        },
        getEmployeeLoadStatus(state){
            return state.EmployeeLoadStatus;
        },
        getEmployeeDeleteStatus(state){
            return state.EmployeeDeleteStatus;
        },

    }

};