window._ = require('lodash');
window.Popper = require('popper.js').default;


try {
    window.$ = window.jQuery = require('jquery');

} catch (e) {}

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


import Vue from 'vue';
import router from './routes';
import store from './store';

//importing vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.config.devtools=true;

Vue.use(Vuetify);
const vuetifyOptions={
    theme:{
        themes:{
            light:{
                primary: '#3f3f3f',
                secondary: '#5b5b5b',
                tertiary: '#001F56',
                other: '#00398C',
                accent: '#f9f9f9',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107'
            }
        }
    }};

//importing ckeditor
import CKEditor from '@ckeditor/ckeditor5-vue'
Vue.use(CKEditor);

//import smoothscroll
// import vueSmoothScroll from 'vue2-smooth-scroll'
// Vue.use(vueSmoothScroll);

// vue and cookies
import VueCookies from 'vue-cookies-ts'
Vue.use(VueCookies);


//authentication
router.beforeEach((to,from,next)=>{
    //if the user is saved in the cookie then log them in
    if(Vue.cookies.isKey('user') && store.getters.getUserLoginStatus!==2){
    store.dispatch('forceLogin',{
        user:Vue.cookies.get('user')
    });

    //if the user is not saved in the cookie then log them out
}else if(Vue.cookies.isKey('user')===false){
    store.dispatch('logoutUser')
}

//check if the router is guest or auth
if (to.matched.some(record=>record.meta.loginRoute)){
    if(store.getters.getUserLoginStatus!==2){
        if(store.getters.getEmployerLoadStatus!==2)
            next('/');
        else
            next()
    }
    else
        next(false);
}else if (to.matched.some(record=>record.meta.guest)){
    if(store.getters.getUserLoginStatus===2){
        if(from.name===null) {
            if(store.getters.getAdmin!==true){
                next('/dashboard')
            }else
                next('/dashboard')

        }else
            next(false);
    }
    else{
        next()
    }
}else if (to.matched.some(record=>record.meta.auth)){
    if(store.getters.getUserLoginStatus!==2){
        if(store.getters.getEmployerLoadStatus!==2)
            next('/');
        else
            next('/login')
    }
    else{
        if(to.matched.some(record=>record.meta.admin)){
            if(store.getters.getAdmin!==true){
                next(false)
            }else
                next()
        }else{
            if(store.getters.getAdmin===true){
                next(false)
            }else{
                if(to.matched.some(record=>record.meta.eligibility)){
                    if(store.getters.getUserEligibility!==true){
                        next(from)
                    }else
                        next()
                }else
                    next()
            }
        }
    }

}else {
    next()
}
});



new Vue({
    router,
    store,
    vuetify:new Vuetify(vuetifyOptions)
}).$mount('#app');

// Vue.config.devtools=true;

