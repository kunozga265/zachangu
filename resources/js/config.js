/*
    Defines the API route we are using.
*/
let api_url = '';

switch( process.env.NODE_ENV ){
    case 'development':
        api_url = 'http://localhost:8000/api';
        break;
    case 'production':
        // api_url = 'https://app.zachanguloans.com/api';
        api_url = 'https://console.zachanguloans.com/api';
        break;
}

export const API = {
    API_URL: api_url
};
