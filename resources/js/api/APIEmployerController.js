/*
    Imports the API URL from the config.
*/
import { API } from '../config.js';
let controller='employers';

export default {
    /*
        GET
    */
    index: function(){
        return axios.get( API.API_URL + '/' + controller + '/');
    },

    /*
        GET
    */
    show: function(data){
        return axios.get( API.API_URL + '/' + controller + '/'+data.id, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
      POST
    */
    store: function(data){
        return axios.post( API.API_URL + '/' + controller,
            {
                name    : data.name,
                address : data.address,
                letter : data.letter,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },

    /*
      POST
    */
    update: function( data){
        return axios.post( API.API_URL + '/' + controller + '/' + data.id,
            {
                name    : data.name,
                address : data.address,
                letter : data.letter,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },
    /*
      DELETE
    */
    destroy: function( data){
        return axios.delete( API.API_URL + '/' + controller + '/'+data.id, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

}