/*
    Imports the API URL from the config.
*/
import { API } from '../config.js';
let controller='users';

export default {
    /*
        GET
    */
    eligibility: function(data){
        return axios.get( API.API_URL + '/' + controller + '/'+data.id + '/eligibility', {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },
 /*
        GET
    */
    index: function(data,page){
        return axios.get( API.API_URL + '/' + controller + '?page='+page, {
            headers:{"Authorization":"Bearer "+data.accessToken
            }
        });
    },

    /*
        GET
    */
    show: function(data){
        return axios.get( API.API_URL + '/' + controller + '/'+data.id, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
      POST
    */
    store: function(data){
        return axios.post( API.API_URL + '/' + controller,
            {
              first_name:data.firstName,
              middle_name:data.middleName,
              last_name:data.lastName,
              national_id_number:data.nationalIdNumber,
              email:data.email,
              password:data.password,
              employer_id:data.employerId,
            }
        );
    },
    /*
      POST
    */
    login: function(data){
        return axios.post( API.API_URL + '/' + controller + '/login/attempt',
            {
                email:data.email,
                password:data.password,
                employer_id:data.employerId,
            }
        );
    },

    /*
      POST
    */
    update: function( data){
        return axios.post( API.API_URL + '/' + controller + '/' + data.id,
            {
                type:data.type,
                first_name:data.firstName,
                middle_name:data.middleName,
                last_name:data.lastName,
                new_email:data.newEmail,
                old_email:data.oldEmail,
                national_id_number:data.nationalIdNumber,
                new_password:data.newPassword,
                old_password:data.oldPassword,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },
    /*
      DELETE
    */
    destroy: function( data){
        return axios.delete( API.API_URL + '/' + controller + '/'+data.id, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
      POST / Feedback
    */
    feedback: function(data){
        return axios.post( API.API_URL + '/' + controller + '/feedback/send',
            {
                email:data.email,
                name:data.name,
                message:data.message,
            }
        );
    },

}