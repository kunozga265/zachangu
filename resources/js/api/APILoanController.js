/*
    Imports the API URL from the config.
*/
import { API } from '../config.js';
let controller='loans';

export default {
    /*
        GET
    */
    indexAdmin: function(data){
        return axios.get( API.API_URL + '/admin/' + controller + '/', {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },
    /*
        GET
    */
    indexUserAdmin: function(data,page){
        return axios.get( API.API_URL + '/admin/' + controller + '/' +data.id+'?page='+page, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },
    /*
        GET
    */
    index: function(data){
        return axios.get( API.API_URL + '/' + controller + '/'+ data.userId +'/' , {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
        GET
    */
    show: function(data){
        return axios.get( API.API_URL + '/' + controller + '/'+ data.userId +'/' +data.code +'/' +data.type +'/get', {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
      POST
    */
    store: function(data){
        return axios.post( API.API_URL + '/' + controller,
            {
                photo:data.photo,
                first_name:data.firstName,
                middle_name:data.middleName,
                last_name:data.lastName,
                phone_number_mobile:data.phoneNumberMobile,
                phone_number_work:data.phoneNumberWork,
                email:data.email,
                physical_address:data.physicalAddress,

                work_address:data.workAddress,
                position:data.position,
                contract_duration:data.contractDuration,
                pay_day:data.payDay,
                pay_slip:data.paySlip,
                // gross:data.gross,
                // net:data.net,
                contract:data.contract,
                reference_letter:data.referenceLetter,
                co_workers:data.coWorkers,

                // bank_statement:data.bankStatement,
                // bank_name:data.bankName,
                // bank_service_center:data.bankServiceCenter,
                // bank_account_name:data.bankAccountName,
                // bank_account_number:data.bankAccountNumber,

                national_id_number:data.nationalIdNumber,
                national_id:data.nationalId,
                passport_id:data.passportId,
                licence_id:data.licenceId,
                other_id:data.otherId,
                amount:data.amount,

                user_id:data.userId,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },
    /*
      POST
    */
    storeSubscribed: function(data){
        return axios.post( API.API_URL + '/' + controller + '/subscribed',
            {
                amount:data.amount,
                user_id:data.userId,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },

    /*
      POST
    */
    modify: function(data){
        return axios.get( API.API_URL + '/admin/' + controller + '/'+data.code+ '/'  + data.type,
            {headers:{"Authorization":"Bearer "+data.accessToken}}
        );
    },

    /*
      POST
    */
    update: function( data){
        return axios.post( API.API_URL + '/' + controller + '/'+ data.userId +'/'  + data.code,
            {
                type:data.type,
                photo:data.photo,
                first_name:data.firstName,
                middle_name:data.middleName,
                last_name:data.lastName,
                phone_number_mobile:data.phoneNumberMobile,
                phone_number_work:data.phoneNumberWork,
                email:data.email,
                physical_address:data.physicalAddress,

                work_address:data.workAddress,
                position:data.position,
                contract_duration:data.contractDuration,
                pay_day:data.payDay,
                pay_slip:data.paySlip,
                // gross:data.gross,
                // net:data.net,
                contract:data.contract,
                reference_letter:data.referenceLetter,
                co_workers:data.coWorkers,

                // bank_statement:data.bankStatement,
                // bank_name:data.bankName,
                // bank_service_center:data.bankServiceCenter,
                // bank_account_name:data.bankAccountName,
                // bank_account_number:data.bankAccountNumber,

                national_id:data.nationalId,
                passport_id:data.passportId,
                licence_id:data.licenceId,
                other_id:data.otherId,
                amount:data.amount,

                consent:data.consent,
                terms_and_conditions:data.termsAndConditions,

            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },
    /*
      DELETE
    */
    destroy: function( data){
        return axios.delete( API.API_URL + '/' + controller + '/'+ data.userId +'/' +data.code, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
      POST
    */
    upload: function( data){
        return axios.post( API.API_URL + '/' + controller + '/upload/file',
            {
                name:data.name,
                file:data.file,
                type:data.type,
            },
            {
                headers:{
                    "Authorization":"Bearer "+ data.accessToken
                }
            }
        );
    },


    /*
        GET
    */
    exportPDF: function(data){
        return axios({
           method:'get',
           url:API.API_URL + '/' + controller +'/export/pdf/' +data.code,
           responseType:'blob',
           headers:{
               "Authorization":"Bearer "+data.accessToken
           }
        });
    },
    /*
        GET
    */
    exportDatasheet: function(data){
        return axios({
           method:'get',
           url:API.API_URL + '/' + controller +'/export/datasheet/' +data.date,
           responseType:'blob',
           headers:{
               "Authorization":"Bearer "+data.accessToken
           }
        });
    },

}