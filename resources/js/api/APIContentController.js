/*
    Imports the API URL from the config.
*/
import { API } from '../config.js';
let controller='pages';

export default {
    /*
        GET
    */
    index: function(){
        return axios.get( API.API_URL + '/' + controller + '/');
    },


    /*
      POST
    */
    store: function(data){
        return axios.post( API.API_URL + '/' + controller,
            {
                contents    : data.contents,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },

}