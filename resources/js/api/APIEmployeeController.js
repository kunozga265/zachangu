/*
    Imports the API URL from the config.
*/
import { API } from '../config.js';
let controller='employees';

export default {
    /*
        GET
    */
    index: function(data){
        return axios.get( API.API_URL + '/' + controller + '/', {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
        GET
    */
    show: function(data){
        return axios.get( API.API_URL + '/' + controller + '/'+data.id, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

    /*
      POST
    */
    store: function(data){
        return axios.post( API.API_URL + '/' + controller ,
            {
                photo:data.photo,
                first_name:data.firstName,
                middle_name:data.middleName,
                last_name:data.lastName,
                phone_number_mobile:data.phoneNumberMobile,
                phone_number_work:data.phoneNumberWork,
                position:data.position,
                email:data.email,
                physical_address:data.physicalAddress,
                work_address:data.workAddress,
                national_id_number:data.nationalIdNumber,
                contract_duration:data.contractDuration,
                employer_id:data.employerId,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },

    /*
      POST
    */
    update: function( data){
        return axios.post( API.API_URL + '/' + controller + '/' + data.id,
            {
                photo:data.photo,
                first_name:data.firstName,
                middle_name:data.middleName,
                last_name:data.lastName,
                phone_number_mobile:data.phoneNumberMobile,
                phone_number_work:data.phoneNumberWork,
                position:data.position,
                email:data.email,
                physical_address:data.physicalAddress,
                work_address:data.workAddress,
                national_id_number:data.nationalIdNumber,
                contract_duration:data.contractDuration,
                employer_id:data.employerId,
            }, {
                headers:{"Authorization":"Bearer "+data.accessToken}
            }
        );
    },
    /*
      DELETE
    */
    destroy: function( data){
        return axios.delete( API.API_URL + '/' + controller + '/'+data.id, {
            headers:{"Authorization":"Bearer "+data.accessToken}
        });
    },

}