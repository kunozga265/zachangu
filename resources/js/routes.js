/*
|-------------------------------------------------------------------------------
| routes.js
|-------------------------------------------------------------------------------
| Contains all of the routes for the application
*/

/*
    Imports Vue and VueRouter to extend with the routes.
*/
import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from './web/pages/Layout.vue';
import Home from './web/pages/Home.vue';
import AdminLogin from './cms/pages/AdminLogin.vue';
import Login from './web/pages/Login.vue';
import Dashboard from './web/pages/Dashboard.vue';
import NewLoanApplication from './web/pages/NewLoanApplication.vue';
import UpdateLoanApplication from './web/pages/UpdateLoanApplication.vue';
import Loan from './web/pages/Loan.vue';
import AdminLayout from './cms/pages/AdminLayout.vue'
import AdminDashboard from './cms/pages/AdminDashboard.vue'
import Employers from './cms/pages/Employers.vue'
import Employer from './cms/pages/Employer.vue'
import NewEmployer from './cms/pages/NewEmployer.vue'
import UpdateEmployer from './cms/pages/UpdateEmployer.vue'
import NewEmployee from './cms/pages/NewEmployee.vue'
import UpdateEmployee from './cms/pages/UpdateEmployee.vue'
import Users from './cms/pages/Users.vue'
import User from './cms/pages/User.vue'
import LoanAdmin from './cms/pages/Loan.vue'
import Contents from './cms/pages/Contents.vue'
// import ApproveLoan from './cms/pages/ApproveLoan.vue'


/*
    Extends Vue to use Vue Router
*/
Vue.use( VueRouter );

/*
    Makes a new VueRouter that we will use to run all of the routes
    for the app.
*/

export default new VueRouter({
    routes: [
        // {
        //     path: '/',
        //     component: Layout,
        //     children: [
        //         {
        //             path: '/',
        //             name: 'home',
        //             component: Home,
        //             meta:{
        //                 guest:true,
        //             },
        //         },
        //         {
        //             path: '/login',
        //             name: 'login',
        //             component: Login,
        //             meta:{
        //                 loginRoute:true,
        //             },
        //         },
        //         {
        //             path: '/dashboard',
        //             name: 'dashboard',
        //             component: Dashboard,
        //             meta:{
        //                 auth:true
        //             },
        //         },
        //         {
        //             path: '/loan-application-form',
        //             name: 'loan-application-form',
        //             component: NewLoanApplication,
        //             meta:{
        //                 auth:true,
        //                 eligibility:true,
        //             },
        //         },
        //         {
        //             path: '/loan/:code',
        //             name: 'loan',
        //             component: Loan,
        //             props:true,
        //             meta:{
        //                 auth:true
        //             },
        //         },
        //         {
        //             path: '/loan/:code/update',
        //             name: 'loan-update',
        //             component: UpdateLoanApplication,
        //             props:true,
        //             meta:{
        //                 auth:true
        //             },
        //         },
        //
        //     ]
        // },
        {
            path: '/',
            name: 'admin-login',
            component: AdminLogin,
            meta:{
                guest:true,
                admin:true
            },
        },
        {
            path: '/',
            component: AdminLayout,
            meta:{
                auth:true,
                admin:true
            },
            children: [
                {
                    path: '/dashboard',
                    name: 'admin-dashboard',
                    component: AdminDashboard,
                    meta:{

                    },
                },
                {
                    path: 'contents',
                    name: 'contents',
                    component: Contents,
                    meta:{

                    },
                },
                {
                    path: 'employers',
                    name: 'employers',
                    component: Employers,
                    meta:{

                    },
                },
                {
                    path: 'new-employer/',
                    name: 'new-employer',
                    component: NewEmployer,
                    meta:{

                    },
                },
                {
                    path: 'employers/:id/update',
                    name: 'update-employer',
                    component: UpdateEmployer,
                    props:true,
                    meta:{

                    },
                },
                {
                    path: 'employers/:id',
                    name: 'employer',
                    component: Employer,
                    props:true,
                    meta:{

                    },
                },
                {
                    path: 'employers/:id/new-employee',
                    name: 'new-employee',
                    component: NewEmployee,
                    props:true,
                    meta:{

                    },
                },
                {
                    path: 'employee/:id/update',
                    name: 'update-employee',
                    component: UpdateEmployee,
                    props:true,
                    meta:{

                    },
                },
                {
                    path: 'users',
                    name: 'users',
                    component: Users,
                    meta:{

                    },
                },
                {
                    path: 'users/:id',
                    name: 'user',
                    component: User,
                    props:true,
                    meta:{

                    },
                },
                {
                    path: 'loan/:code',
                    name: 'loan-admin',
                    component: LoanAdmin,
                    props:true,
                    meta:{
                        auth:true
                    },
                },
                // {
                //     path: 'loan/:code/approve',
                //     name: 'loan-approve',
                //     component: ApproveLoan,
                //     props:true,
                //     meta:{
                //         auth:true
                //     },
                // },
            ]
        }
    ]
});
