<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            //personal information
            $table->string('photo')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone_number_mobile')->nullable();
            $table->string('phone_number_work')->nullable();
            $table->string('email')->nullable();
            $table->json('physical_address')->nullable();

            //Workplace information
            $table->string('position')->nullable();
            $table->json('work_address')->nullable();
            $table->string('contract_duration')->nullable();
            $table->integer('pay_day')->nullable();
            $table->double('gross')->nullable();
            $table->double('net')->nullable();
            $table->string('contract')->nullable();
            $table->string('pay_slip')->nullable();
            $table->string('reference_letter')->nullable();
            $table->json('co_workers')->nullable();


            //bank information
            $table->string('bank_statement')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_service_center')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->integer('bank_account_number')->nullable();

            //identifications
            $table->string('national_id_number')->nullable();
            $table->string('national_id')->nullable();
            $table->string('passport_id')->nullable();
            $table->string('licence_id')->nullable();
            $table->string('other_id')->nullable();

            $table->string('consent')->nullable();
            $table->string('progress')->nullable();
            $table->string('score')->nullable();
           // $table->integer('subscription')->nullable();
            $table->string('terms_and_conditions')->nullable();
            $table->integer('amount');
            $table->double('approved_date')->nullable();
            $table->double('due_date')->nullable();


            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
