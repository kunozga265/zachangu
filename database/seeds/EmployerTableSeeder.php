<?php

use Illuminate\Database\Seeder;

class EmployerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employer=new \App\Employer([
           'name'   =>  'Escom Limited',
           'address'   =>  'Chayamba Building P. O. Box 234 Blantyre 3',
        ]);
        $employer->save();

        $employer=new \App\Employer([
           'name'       =>  'Modern Elevators Limited',
           'address'    =>  'Chipember Highway P. O. Box 234 Blantyre 3',
        ]);
        $employer->save();

        $employer=new \App\Employer([
           'name'   =>  'Fresh Fisheries',
           'address'   =>  'Greenish Corner P. O. Box 234 Blantyre 3',
        ]);
        $employer->save();

    }
}
