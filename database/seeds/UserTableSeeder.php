<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=new \App\User([
            'first_name'            => 'Admin',
            'last_name'             => 'User',
            'national_id_number'    => '@@@@@@@@',
            'email'                 => 'admin@zachanguloans.com',
            'password'              => bcrypt('zachanguloans'),
            'employer_id'           =>  0
        ]);
        $user->save();
    }
}
