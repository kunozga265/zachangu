<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contents=[
            'home'=>[
                'title'=>'Quick and Reliable',
                'subtitle'=>'Apply for a loan with us',
                'contactInformation'=>[
                    'address'=>'Address Name P. O. Box 123 Location',
                    'phoneNumber'=>'+265 123 45 67 89',
                    'email'=>'admin@zachanguloans.com',
                ],
                'about'=>[
                    'title'=>'About us',
                    'body'=>'This is the about us body',
                ],
            ],
            'welcome'=>[
                'title'=>'Welcome to online loans',
                'subtitle'=>'This is the welcome page subtitle',
            ],
            'dashboard'=>[
                'title'=>'Get quick loans with us',
                'subtitle'=>'This is the subtitle ',
            ],
            'newForm'=>[
                'title'=>'New Application Form',
                'subtitle'=>'This is the subtitle ',
            ],
            'updateForm'=>[
                'title'=>'Update Application Form',
                'subtitle'=>'This is the subtitle ',
            ],
            'termsAndConditions'=>[
                'text'=>"<p><strong>ZACHANGU MICROFINANCE AGENCY</strong></p><p><strong>Payday Loan Agreement</strong></p><p><strong>Issued on: __________________</strong></p><p>This Loan Agreement (this “Agreement”), is executed as of this _____ day of __________, ________ (the “Effective Date”)&nbsp;</p><p>By and between&nbsp;</p><p>________________, located at _______________________________________________, hereinafter referred to as the “Borrower”;</p><p>And&nbsp;</p><p><strong>ZACHANGU MICROFINANCE AGENCY</strong>, located at <strong>MANJA TOWNSHIP</strong>, hereinafter referred to as the “Lender”;&nbsp;</p><p><strong>WHEREAS</strong> at the request of the Borrower, the Lender has agreed to grant a Payday Loan of __________________________ to the Borrower till ___________________________on terms and conditions hereinafter contained.</p><p>The parties agree as follows:&nbsp;</p><p>1. <strong>Loan Amount</strong>: The Lender agrees to loan the Borrower the principal sum of ___________________ (the “Loan”).&nbsp;</p><p>2. <strong>Interest:</strong> The payday loan bears interest at the rate of __10__% till the Borrower’s day of receiving salary: ____________________</p><p>Calculations are as follows:&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>3. <strong>Repayment of Loan</strong>: The Loan, together with accrued and unpaid interest and all other charges, costs and expenses, is due and payable on or before ______________________.&nbsp;</p><p>4. <strong>Penalty:</strong> Where the employer fails to honor the date of repayment for the loan, he/she will be given three days as grace period. If the Borrower fails to pay within the grace period of three days, after the three days, each day the borrower agrees to pay 10% of the due payment till payment is made. The Borrower will also pay to the Lender all charges the Lender met to collect the overdue payment.&nbsp;</p><p>5. <strong>Guarantee:</strong> Guarantor’s Full Name located at Guarantor’s Complete Address (the “Guarantor”) promises to unconditionally guarantee to the Lender, the full payment and performance by Borrower of all duties and obligations arising under this Agreement. The Guarantor agrees that this guarantee shall remain in full force and effect and be binding on the Guarantor until this Agreement is satisfied.&nbsp;</p><p>6. <strong>Prepayment:</strong> The Borrower has the right to prepay all or any part of the Loan, together with accrued and unpaid interest thereon, at any time without prepayment penalty or premium of any kind.&nbsp;</p><p>7. <strong>Costs and Expenses</strong>: Borrower shall pay to the Lender all costs of collection, including reasonable attorney's fees, the Lender incurs in enforcing this Agreement.&nbsp;</p><p>8. <strong>Waiver:</strong> The Borrower and all sureties, guarantors and endorsers here of, waive presentment, protest and demand, notice of protest, demand and dishonor and nonpayment of this Agreement.&nbsp;</p><p>9. <strong>Successors and Assigns</strong>: This Agreement will inure to the benefit of and be binding on the respective successors and permitted assigns of the Lender and the Borrower.&nbsp;</p><p>10. <strong>Amendment:</strong> This Agreement may be amended or modified only by a written agreement, duly signed by both the Borrower and the Lender.&nbsp;</p><p>11. <strong>Notices:</strong> Any notice or communication under this Loan must be in writing and sent via email.</p><p>12. <strong>No Waiver:</strong> Lender shall not be deemed to have waived any provision of this Agreement or the exercise of any rights held under this Agreement unless such waiver is made expressly and in writing. Waiver by Lender of a breach or violation of any provision of this Agreement shall not constitute a waiver of any other subsequent breach or violation.&nbsp;</p><p>13. <strong>Severability:</strong> In the event that any of the provisions of this Agreement are held to be invalid or unenforceable in whole or in part, the remaining provisions shall not be affected and shall continue to be valid and enforceable as though the invalid or unenforceable parts had not been included in this Agreement.&nbsp;</p><p>14. <strong>Assignment:</strong> Borrower shall not assign this Agreement, in whole or in part, without the written consent of Lender. Lender may assign all or any portion of this Agreement with written notice to Borrower.&nbsp;</p><p>15. <strong>Governing Law:</strong> This Agreement shall be governed by and construed in accordance with the laws of the Republic of Malawi, not including its conflicts of law provisions.&nbsp;</p><p>16. <strong>Disputes:</strong> Any dispute arising from this Agreement shall be resolved in the courts of the Republic of Malawi.&nbsp;</p><p>17. <strong>Entire Agreement:</strong> This Agreement contains the entire understanding between the parties and supersedes and cancels all prior agreements of the parties, whether oral or written, with respect to such subject matter.&nbsp;</p><p>IN WITNESS WHEREOF, the parties have executed this Agreement as of the date first stated above.</p>",
                'file'=>'files/terms_and_conditions.pdf'
            ],
            'amountLimit'=>[
                'lower'=>5000,
                'upper'=>10000,
            ],
            'fee'=>510,
            'interest'=>0.06,

        ];
        $page=new \App\Page([
            'contents'=>json_encode($contents)
        ]);

        $page->save();
    }
}
